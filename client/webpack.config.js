var path = require("path");
var htmlPlugin = require("html-webpack-plugin");
var Dotenv = require("dotenv-webpack");

module.exports = (env, argv) => {
  return {
    context: path.join(__dirname, "./src"),
    mode: "development",
    entry: "./app.tsx",
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx"]
    },
    module: {
      rules: [
        {
          test: /\.(tsx|ts)?$/,
          use: "ts-loader",
          exclude: /node_modules/
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: ["babel-loader"]
        },
        {
          test: /\.scss$/,
          use: ["style-loader", "css-loader", "sass-loader"]
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        },
        {
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          loader: "graphql-tag/loader"
        }
      ]
    },
    plugins: [
      new htmlPlugin({
        template: "index.html"
      }),
      new Dotenv({
        path: argv.mode === "production" ? "./.env" : "./.env.staging"
      })
    ],
    // Run optimisation scripts depending on the `mode` (dev or prod).
    // webpack minimises the code by default on prod
    optimization: {
      splitChunks: {
        chunks: "all"
      }
    },
    node: {
      fs: "empty",
      net: "empty",
      tls: "empty"
    },
    devServer: {
      // Allows to handle routes with React instead of webpack
      historyApiFallback: true
    },
    devtool: "source-map"
  };
};
