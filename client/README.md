# FullCalendar React+TypeScript Example Project

This is a fully-buildable example project for FullCalendar, React, and TypeScript, leveraging Webpack and Sass.

## Installation

```bash
git clone https://github.com/fullcalendar/fullcalendar-example-projects.git
cd fullcalendar-example-projects/react-typescript
yarn install
```

## Build Commands

```bash
yarn run build
yarn run watch
yarn run clean
```

After running `build` or `watch`, open up `index.html` in a browser.
