import FullCalendar from "@fullcalendar/react";
import { EventInput } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import * as React from "react";

const frLocale = require("@fullcalendar/core/locales/fr");
const ical = require("ical");
// const fs = require("fs");
import * as fs from "fs";

import "./style.scss";

interface IAgenda {
  id: string;
  name: string;
  url: string;
}

interface IProps {
  agendas: IAgenda[];
}

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];

const Calendar = ({ agendas }: IProps) => {
  const calendarComponentRef = React.createRef<FullCalendar>();
  const [events, setEvents] = React.useReducer((events, { type, value }) => {
    switch (type) {
      case "add":
        return [value, ...events];
      case "init":
        return [...value];
      case "merge":
        return [...value, ...events];
      case "remove":
        return events.filter((_: any, index: number) => index !== value);
      default:
        return events;
    }
  }, []);

  const addEvents = (data: any) => {
    for (let k in data) {
      if (data.hasOwnProperty(k)) {
        const event = data[k];
        if (event.type == "VEVENT") {
          setEvents({
            type: "add",
            value: {
              id: event.uid,
              title: event.summary,
              start: event.start,
              end: event.end,
              description: event.description
            }
          });
        }
      }
    }
  };

  React.useEffect(() => {
    agendas.map((agenda: any) => {
      ical.fromURL(
        `https://cors-anywhere.herokuapp.com/${agenda.url}`,
        {},
        (err: any, data: any) => {
          // ical.fromURL(agenda.url, { mode: "no-cors" }, (err: any, data: any) => {
          addEvents(data);
        }
      );

      //       const calendar = `BEGIN:VCALENDAR
      // METHOD:PUBLISH
      // VERSION:2.0
      // PRODID:-//Assemblee-Nationale//iCal 3.0//FR
      // TZ:+00
      //
      // BEGIN:VCALENDAR
      // METHOD:PUBLISH
      // VERSION:2.0
      // PRODID:-//Assemblee-Nationale//iCal 3.0//FR
      // TZ:+00
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432483
      // DTSTART:20190513T134500Z
      // SUMMARY:Réunion de la commission des lois
      // LOCATION:salle 6242 (Lois)
      //
      // DESCRIPTION:Entrée en fonction des représentants au Parlement européen élus en France aux élections de 2019 (n° 1880) (amendements, art. 88) ;\nTransformation de la fonction publique (n° 1802) (amendements, art. 88).\n
      // DTEND:20190513T135000Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDS21618
      // DTSTART:20190513T140000Z
      // SUMMARY:Première séance publique (228e)
      // LOCATION:Assemblée nationale
      //
      // DESCRIPTION:Discussion du projet de loi relatif à l'entrée en fonction des représentants au Parlement européen élus en France aux élections de 2019\nDiscussion du projet de loi de transformation de la fonction publique\n
      // DTEND:20190513T175500Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432453
      // DTSTART:20190513T140000Z
      // SUMMARY:Réunion de la mission d'information sur les relations entre les grands donneurs d'ordre et les sous-traitants dans les filières industrielles
      // LOCATION:salon Mars 1 (32 rue Saint-Dominique)
      //
      // DESCRIPTION:Audition de M. Jean-Cedric Bekale et de M. Jacques-Hermann Ntoko, co-fondateurs de la société CEO TradeIn.
      //
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432501
      // DTSTART:20190513T140000Z
      // SUMMARY:Réunion de la commission d'enquête sur la situation et les pratiques de la grande distribution et de ses groupements dans leurs relations commerciales avec les fournisseurs
      // LOCATION:salle Lamartine
      //
      // DESCRIPTION:Audition, ouverte à la presse, Mme Marie-Thérèse Bonneau, vice-présidente de la Fédération Nationale des Producteurs de Lait (FNPL), accompagnée de M. Vincent Brack, directeur.
      //
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432687
      // DTSTART:20190513T160000Z
      // SUMMARY:Réunion de la mission d'information sur les relations entre les grands donneurs d'ordre et les sous-traitants dans les filières industrielles
      // LOCATION:salon Mars 1 (101 rue de l'Université)
      //
      // DESCRIPTION:Audition de M. Frédéric Amblard, directeur de Factory Lab.
      //
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDS21619
      // DTSTART:20190513T193000Z
      // SUMMARY:Deuxième séance publique (229e)
      // LOCATION:Assemblée nationale
      //
      // DESCRIPTION:Suite de la discussion du projet de loi de transformation de la fonction publique\n\n
      // DTEND:20190513T225000Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432604
      // DTSTART:20190514T064500Z
      // SUMMARY:Réunion de la mission de suivi de l'évaluation de la politique d'accueil touristique
      // LOCATION:Salle du CEC
      //
      // DESCRIPTION:Audition de M. Christophe Decloux, directeur général du Comité régional du tourisme (CRT) Paris Île-de-France.
      // DTEND:20190514T074500Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432665
      // DTSTART:20190514T073000Z
      // SUMMARY:Réunion de la
      // LOCATION:salle 6822, 3ème étage, 126, rue de l'Université
      //
      // DESCRIPTION:Audition de Mme Geneviève Avenard, défenseure des enfants et adjointe en charge de la défense et de la promotion des droits de l'enfant, Mme Marie Lieberherr, cheffe du pôle défense des droits de l'enfant, Mme Stéphanie Carrère, conseillère en charge de l'international au cabinet du Défenseur des droits et Mme France de Saint Martin, attachée parlementaire du Défenseur des droits.
      //
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432605
      // DTSTART:20190514T074500Z
      // SUMMARY:Réunion de la mission de suivi de l'évaluation de la politique d'accueil touristique
      // LOCATION:Salle du CEC
      //
      // DESCRIPTION:Audition de M. Olivier Lacoste, adjoint au sous-directeur tourisme du ministère de l'économie et des finances.
      // DTEND:20190514T091000Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432318
      // DTSTART:20190514T113000Z
      // SUMMARY:Réunion de la délégation aux collectivités territoriales et à la décentralisation
      // LOCATION:salle 4123 (33 rue Saint-Dominique)
      //
      // DESCRIPTION:Avis sur le projet de loi, adopté par le Sénat, d’orientation pour les mobilités (n° 1831, Mme Monica Michel, rapporteure).
      //
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432606
      // DTSTART:20190514T113000Z
      // SUMMARY:Réunion
      // LOCATION:salle 3 - 95, rue de l'Université
      //
      // DESCRIPTION:Table ronde « innovation dans le tourisme », avec la participation de :\nM. Laurent Queige, délégué général de Welcome City Lab ;\nM. Jean-Rémi Kouchakji, directeur général, et M. Patrick Verlynde, secrétaire général de Payintech.com ;\nM. Romain Viennois, fondateur de Francehostels.fr ;\nM. Hatem Chelbi, président de Tootsweet.app.\n
      //
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432341
      // DTSTART:20190514T120000Z
      // SUMMARY:Réunion de la commission d'enquête sur la situation, les missions et les moyens des forces de sécurité, qu’il s’agisse de la police nationale, de la gendarmerie ou de la police municipale
      // LOCATION:salle Lamartine
      //
      // DESCRIPTION:Table ronde, à huis clos, d’équipementiers : M. Guillaume Verney-Carron, directeur général de la société Verney-Carron et M. Laurent Marck, directeur général du groupe Marck accompagné de Mme Amélie Serey, chargée des relations institutionnelles.
      // DTEND:20190514T130000Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432685
      // DTSTART:20190514T121500Z
      // SUMMARY:Réunion de la mission d'évaluation de la loi du 7 juillet 2016 sur la liberté de la création, l'architecture et le patrimoine
      // LOCATION:4e Bureau
      //
      // DESCRIPTION:Table ronde, non ouverte à la presse :\nUnion nationale des aménageurs (UNAM) : M. Arnaud Pautigny, administrateur de l’UNAM et de l’INRAP, et M. Yann Le Corfec,  directeur juridique de l’UNAM ;\nAssociation nationale pour l’archéologie de collectivité territoriale (ANACT) : Mme Laure Koupaliantz, responsable du service d'archéologie du Grand Reims, et M. Vincent Hincker, responsable du Service d'archéologie du Département du Calvados ;\nSyndicat national des professionnels de l’archéologie (SNPA): M. Frédéric Rossi, président, M. Bertrand Bakaj, de la société Antea, membre du SNPA, M. Ugo Cafiero, de la société Hadès, membre du SNPA ;\nConférence des conservateurs régionaux d’archéologie : M. Stéphane Deschamps, conservateur général du patrimoine au conservatoire régional de l’archéologie, et M. Stéphane Révillion, conservateur général du patrimoine au conservateur régional de l'archéologie de la région Centre-Val de Loire.\n
      // DTEND:20190514T140000Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDS21620
      // DTSTART:20190514T130000Z
      // SUMMARY:Première séance publique (230e)
      // LOCATION:Assemblée nationale
      //
      // DESCRIPTION:Questions au Gouvernement\nSuite de la discussion du projet de loi de transformation de la fonction publique\n
      // DTEND:20190514T180000Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432028
      // DTSTART:20190514T141500Z
      // SUMMARY:Réunion de la mission d'information commune sur les perturbateurs endocriniens présents dans les contenants en plastique
      // LOCATION:salle 6566 (Lois)
      //
      // DESCRIPTION:Audition de M. Roland Caigneaux et de M. Thibault Leroux, réseau santé-environnement de France nature environnement (FNE) ; de M. François Veillerette, directeur de Générations futures, et de Mme Fleur Gorre, chargée de mission risque chimique ; de Mme Élisabeth Ruffinengo, responsable plaidoyer et projets santé-environnement de Women in Europe for a common future (WECF France).
      // DTEND:20190514T151500Z
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // BEGIN:VEVENT
      // UID:OMC_RUANR5L15S2019IDC432607
      // DTSTART:20190514T141500Z
      // SUMMARY:Réunion
      // LOCATION:salle 3 - 95, rue de l'Université
      //
      // DESCRIPTION:Audition de Mme Vanessa Heydorff, directrice pour la France, l’Espagne et le Portugal de Booking.com, accompagnée de M. Alexis Darmois et de Mme Mathilde Moch, conseils de Booking.com.
      //
      // TRANSP:TRANSPARENT
      // END:VEVENT
      //
      // END:VCALENDAR`;
      //
      //       const data = ical.parseICS(calendar);
      // const data = ical.parseICS(fs.readFileSync("./mockdata/agenda.ics"));

      // console.log(data);

      const data: any = [];
    });
  }, [setEvents, agendas]);

  const gotoPast = () => {
    let calendarApi = calendarComponentRef.current!.getApi();
    calendarApi.gotoDate("2000-01-01"); // call a method on the Calendar object
  };

  return (
    <FullCalendar
      defaultView="dayGridMonth"
      events={events}
      header={{
        left: "prev,next today",
        center: "title",
        right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek"
      }}
      locale={frLocale}
      plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
      ref={calendarComponentRef}
    />
  );
};

export default Calendar;
