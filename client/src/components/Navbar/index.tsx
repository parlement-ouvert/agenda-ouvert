import { Alignment, Button, Navbar as BPNavbar } from "@blueprintjs/core";
import * as React from "react";
import useReactRouter from "use-react-router";

import "./style.scss";

interface IProps {
  rightComponent?: any;
}

const Navbar = ({ rightComponent }: IProps) => {
  const { history } = useReactRouter();

  return (
    <BPNavbar id="navbar" className="bp3-dark">
      <BPNavbar.Group align={Alignment.LEFT}>
        <BPNavbar.Heading onClick={() => history.push("/")}>
          <h2>PARLEMENT OUVERT</h2>
        </BPNavbar.Heading>
      </BPNavbar.Group>
      <BPNavbar.Group align={Alignment.RIGHT}>{rightComponent}</BPNavbar.Group>
    </BPNavbar>
  );
};

export default Navbar;
