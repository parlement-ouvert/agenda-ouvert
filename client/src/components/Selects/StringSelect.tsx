import * as React from "react";
import { Button, MenuItem, Intent, IPopoverProps } from "@blueprintjs/core";
import { Select, ItemPredicate, ItemRenderer } from "@blueprintjs/select";
import { IconName } from "@blueprintjs/icons";

interface ISelectProps {
  disabled?: boolean;
  icon?: IconName;
  inputItem: string;
  intent?: Intent;
  items: string[];
  large?: boolean;
  loading?: boolean;
  onChange: (item: string) => void;
  popoverProps?: IPopoverProps;
}

const StringSelect = ({
  disabled,
  icon,
  inputItem,
  intent,
  items,
  large,
  loading,
  onChange,
  popoverProps
}: ISelectProps) => {
  const renderItem: ItemRenderer<string> = (item, { handleClick }) => {
    return <MenuItem key={item} onClick={handleClick} text={item} />;
  };

  const filterByName: ItemPredicate<string> = (query, item) => {
    return `${item.toLowerCase()}`.indexOf(query.toLowerCase()) >= 0;
  };

  const displayItem = (item: string): string => {
    return item ? item : "Vide";
  };

  const CustomSelect = Select.ofType<string>();

  return (
    <CustomSelect
      disabled={disabled}
      items={items}
      itemPredicate={filterByName}
      itemRenderer={renderItem}
      noResults={<MenuItem disabled={true} text="No results." />}
      onItemSelect={onChange}
      popoverProps={popoverProps}
    >
      <Button
        disabled={disabled}
        icon={icon}
        intent={intent ? intent : undefined}
        large={large}
        loading={loading}
        rightIcon="caret-down"
        text={displayItem(inputItem)}
      />
    </CustomSelect>
  );
};

export default StringSelect;
