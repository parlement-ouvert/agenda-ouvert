import { Icon } from "@blueprintjs/core";
import * as React from "react";

import "./style.scss";

interface IProps {
  handleClick: any;
  isPerson: boolean;
  name: string;
  selected: boolean;
}

const AgendaItem = ({ selected, name, isPerson, handleClick }: IProps) => {
  return (
    <div
      className={`agenda ${selected ? "selected" : null}`}
      onClick={handleClick}
    >
      <div className="icon">
        <Icon icon={isPerson ? "user" : "calendar"} />
      </div>
      <div className="info">{name}</div>
    </div>
  );
};

export default AgendaItem;
