import { Button, Spinner, Tab, TabId, Tabs } from "@blueprintjs/core";
import * as React from "react";
import { Query } from "react-apollo";
import { useApolloClient } from "react-apollo-hooks";
import useReactRouter from "use-react-router";

// @ts-ignore
import Pie from "./components/Pie";

import Calendar from "../../components/Calendar";
import Navbar from "../../components/Navbar";
import DeputySelect from "./components/DeputySelect";
import DocumentsViewer from "./components/DocumentsViewer";

import "./style.scss";

const users = require("../../graphql/queries/users.graphql");
const deputyAgendas = require("../../graphql/queries/deputyAgendas.graphql");
const deputyDocuments = require("../../graphql/queries/deputyDocuments.graphql");

interface IAgenda {
  id: string;
  name: string;
  url: string;
}

const MainView = () => {
  const client = useApolloClient();
  const { history } = useReactRouter();

  const [currentTab, setCurrentTab] = React.useState();
  const [currentDeputy, setCurrentDeputy] = React.useState();
  const [deputies, setDeputies] = React.useState([]);

  React.useEffect(() => {
    client
      .query({ query: users })
      .then((response: any) => {
        setDeputies(response.data.users);
      })
      .catch((error: any) => {
        console.log(error);
        throw new Error(error);
      });
  }, [setDeputies]);

  const handleTabChange = (newTabId: TabId) => setCurrentTab(newTabId);

  return (
    <div>
      <Navbar
        rightComponent={
          <Button
            intent="primary"
            icon="person"
            onClick={() => {
              history.push("/register");
            }}
          >
            Je suis parlementaire
          </Button>
        }
      />

      <div id="main-container">
        <div id="deputy-selector">
          <Query
            query={users}
            onCompleted={(data: any) => {
              if (data.users.length > 0) {
                setCurrentDeputy(data.users[0]);
              }
            }}
          >
            {({ data, loading }: any) => {
              return (
                <DeputySelect
                  items={data && data.users ? data.users : []}
                  inputItem={currentDeputy}
                  large={true}
                  loading={loading}
                  onChange={(newDeputy: any) => {
                    setCurrentDeputy(newDeputy);
                  }}
                />
              );
            }}
          </Query>
        </div>

        {currentDeputy ? (
          <div id="tabs">
            <Tabs
              animate={false}
              large={true}
              onChange={handleTabChange}
              selectedTabId={currentTab}
              vertical={true}
            >
              <Tab
                id="spendings"
                title="Dépenses"
                panel={
                  <Query
                    query={deputyDocuments}
                    variables={{ id: currentDeputy.id }}
                  >
                    {({ data, loading }: any) => {
                      return loading ? (
                        <Spinner />
                      ) : data && data.deputyDocuments ? (
                        <DocumentsViewer documents={data.deputyDocuments} />
                      ) : null;
                    }}
                  </Query>
                }
              />
              <Tab
                disabled
                id="agenda"
                title="Agenda"
                panel={
                  <div id="calendar">
                    <Query
                      query={deputyAgendas}
                      variables={{ id: currentDeputy.id }}
                    >
                      {({ data, loading }: any) => {
                        return loading ? (
                          <Spinner />
                        ) : data && data.deputyAgendas ? (
                          <Calendar agendas={data.deputyAgendas} />
                        ) : null;
                      }}
                    </Query>
                  </div>
                }
              />
            </Tabs>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default MainView;
