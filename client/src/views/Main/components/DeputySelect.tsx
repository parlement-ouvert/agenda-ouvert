import * as React from "react";
import { Button, MenuItem, Intent, IPopoverProps } from "@blueprintjs/core";
import { Select, ItemPredicate, ItemRenderer } from "@blueprintjs/select";
import { IconName } from "@blueprintjs/icons";

interface IDeputy {
  id: string;
  name: string;
}

interface ISelectProps {
  disabled?: boolean;
  icon?: IconName;
  inputItem: IDeputy;
  intent?: Intent;
  items: IDeputy[];
  large?: boolean;
  loading?: boolean;
  onChange: (item: IDeputy) => void;
  popoverProps?: IPopoverProps;
}

const DeputySelect = ({
  disabled,
  icon,
  inputItem,
  intent,
  items,
  large,
  loading,
  onChange,
  popoverProps
}: ISelectProps) => {
  const renderItem: ItemRenderer<IDeputy> = (item, { handleClick }) => {
    return <MenuItem key={item.id} onClick={handleClick} text={item.name} />;
  };

  const filterByName: ItemPredicate<IDeputy> = (query, item) => {
    return `${item.name.toLowerCase()}`.indexOf(query.toLowerCase()) >= 0;
  };

  const displayItem = (item: IDeputy): string => {
    return item ? item.name : "Vide";
  };

  const CustomSelect = Select.ofType<IDeputy>();

  return (
    <CustomSelect
      disabled={disabled}
      items={items}
      itemPredicate={filterByName}
      itemRenderer={renderItem}
      noResults={<MenuItem disabled={true} text="No results." />}
      onItemSelect={onChange}
      popoverProps={popoverProps}
    >
      <Button
        disabled={disabled}
        icon={icon}
        intent={intent ? intent : undefined}
        large={large}
        loading={loading}
        rightIcon="caret-down"
        text={displayItem(inputItem)}
      />
    </CustomSelect>
  );
};

export default DeputySelect;
