import * as React from "react";
import { Pie } from "@vx/shape";
import { Group } from "@vx/group";
import { Text } from "@vx/text";

const black = "#000000";

const value = d => d.value;

const CustomPie = ({
  data,
  width,
  height,
  margin,
  onMouseOverArc,
  onMouseOutArc,
  categories
}) => {
  const radius = Math.min(width, height) / 2;
  const centerY = height / 2;
  const centerX = width / 2;

  return (
    <svg width={width} height={height}>
      <Group top={centerY - margin.top} left={centerX}>
        <Pie
          data={data}
          pieValue={value}
          outerRadius={radius - 80}
          innerRadius={radius - 160}
          cornerRadius={0}
          padAngle={0}
        >
          {pie => {
            return pie.arcs.map((arc, i) => {
              const [centroidX, centroidY] = pie.path.centroid(arc);
              const { startAngle, endAngle } = arc;
              const hasSpaceForLabel = endAngle - startAngle >= 0.1;
              const [opacity, setOpacity] = React.useState(0.7);
              return (
                <g
                  key={`browser-${arc.data.label}-${i}`}
                  onMouseOver={() => {
                    setOpacity(0.9);
                    onMouseOverArc(arc.data);
                  }}
                  onMouseOut={() => {
                    setOpacity(0.7);
                    onMouseOutArc();
                  }}
                >
                  <path
                    d={pie.path(arc)}
                    fill={categories[arc.data.type].color}
                    fillOpacity={opacity}
                  />
                  {hasSpaceForLabel && (
                    <Text
                      fill={black}
                      x={centroidX}
                      y={centroidY}
                      width={"300px"}
                      fontSize={15}
                      textAnchor="middle"
                      verticalAnchor="middle"
                    >
                      {categories[arc.data.type].name}
                    </Text>
                  )}
                </g>
              );
            });
          }}
        </Pie>
      </Group>
    </svg>
  );
};

export default CustomPie;
