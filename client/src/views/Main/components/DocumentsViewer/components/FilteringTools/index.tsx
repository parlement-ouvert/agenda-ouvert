import { Icon, IconName } from "@blueprintjs/core";
import * as React from "react";

import "./style.scss";

interface IProps {
  query: string;
  setQuery: any;
  sortBy: string;
  setSortBy: any;
}

const FilteringTools = ({ query, setQuery, sortBy, setSortBy }: IProps) => {
  let dateIcon: IconName = "sort-desc";
  if (sortBy == "date ascending") {
    dateIcon = "sort-asc";
  }
  return (
    <div className="filtering-tools">
      <div
        className={`sorting ${sortBy.startsWith("date") ? "sorted" : ""}`}
        onClick={() => {
          switch (sortBy) {
            case "date ascending":
              setSortBy("date descending");
              break;
            case "date descending":
              setSortBy("date ascending");
              break;
            default:
              setSortBy("date descending");
              break;
          }
        }}
      >
        <Icon icon={dateIcon} />
      </div>
      <div className="bp3-input-group filter-bar">
        <Icon icon="search" />
        <input
          className="bp3-input"
          type="search"
          placeholder="Rechercher..."
          dir="auto"
          value={query || ""}
          onChange={(event: any) => {
            const value = event.target.value;
            setQuery(value);
          }}
        />
      </div>
      <div
        className={`sorting ${sortBy.startsWith("price") ? "sorted" : ""}`}
        onClick={() => {
          switch (sortBy) {
            case "price ascending":
              setSortBy("price descending");
              break;
            case "price descending":
              setSortBy("price ascending");
              break;
            default:
              setSortBy("price descending");
              break;
          }
        }}
      >
        <Icon icon={dateIcon} />
      </div>
    </div>
  );
};

export default FilteringTools;
