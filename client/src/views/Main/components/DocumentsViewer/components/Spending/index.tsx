import * as React from "react";

import "./style.scss";

interface IProps {
  spending: {
    amount: number;
    date: string;
    description: string;
    supplier: string;
    category: string;
  };
  color: string;
}

const Spending = ({ spending, color }: IProps) => {
  const d = new Date(spending.date);

  const twoDigit = (n: number) => `${n < 10 ? "0" : ""}${n}`;

  return (
    <div className="spending-entry">
      <div className="information">
        <div className="date">
          {[
            twoDigit(d.getDate()),
            twoDigit(d.getMonth() + 1),
            d.getFullYear()
          ].join("-")}
        </div>
        <div className="description">
          <span>{spending.description}</span>
          {spending.supplier ? (
            <span className="supplier">{` | ${spending.supplier}`}</span>
          ) : null}
        </div>
      </div>
      <div className="tags">
        <span className="category" style={{ backgroundColor: color }}>
          {spending.category}
        </span>
        <span className="amount">{spending.amount} €</span>
      </div>
    </div>
  );
};

export default Spending;
