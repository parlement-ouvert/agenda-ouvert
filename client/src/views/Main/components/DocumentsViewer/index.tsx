import * as React from "react";
// @ts-ignore
import Pie from "../Pie";
// @ts-ignore
import { ParentSize } from "@vx/responsive";
// @ts-ignore
import { Text } from "@vx/text";
// @ts-ignore
import { Group } from "@vx/group";

import FilteringTools from "./components/FilteringTools";
import Spending from "./components/Spending";

import "./style.scss";

const colors = [
  "#2965CC",
  "#29A634",
  "#D99E0B",
  "#D13913",
  "#8F398F",
  "#00B3A4",
  "#DB2C6F",
  "#9BBF30",
  "#96622D",
  "#7157D9"
];

const categories: any = {
  PERM: { name: "Permanence", color: "#2965CC" },
  MOVE: { name: "Déplacements", color: "#29A634" },
  MEAL: { name: "Hébergement et Repas", color: "#D99E0B" },
  LEARN: { name: "Formations", color: "#D13913" },
  COM: { name: "Frais de Communication et de Documentation", color: "#8F398F" },
  REC: { name: "Frais de Réception et de Représentation", color: "#00B3A4" },
  PPL: { name: "Dépenses de Personnel et de Services", color: "#DB2C6F" },
  END: { name: "Dépenses liées à la fin du mandat", color: "#9BBF30" },
  MISC: { name: "Dépenses Diverses", color: "#96622D" },
  UNJUST: {
    name: "Dépenses payées sans justificatif fourni par un tiers",
    color: "#7157D9"
  }
};
interface IProps {
  documents: any;
}

const DocumentsViewer = ({ documents }: IProps) => {
  const [currentDocument, setCurrentDocument] = React.useState();
  const [currentSubValue, setCurrentSubValue] = React.useState();
  const [totalSpent, setTotalSpent] = React.useState();
  const [query, setQuery] = React.useState();
  const [sortBy, setSortBy] = React.useState("date descending");

  return (
    <div id="documents-viewer">
      <div id="documents-list">
        {documents.map((document: any, index: number) => {
          return (
            <div
              className={`document-item ${
                currentDocument && document.id == currentDocument.id
                  ? "selected"
                  : ""
              }`}
              key={index}
              onClick={() => {
                setCurrentDocument(document);
                setTotalSpent(
                  Math.round(
                    document.metrics
                      .map((metric: any) => metric.value)
                      .reduce((a: number, b: number) => a + b, 0)
                  )
                );
              }}
            >
              <div className="filename">{document.name}</div>
              <div className="date">{document.createdAt.slice(0, 10)}</div>
            </div>
          );
        })}
      </div>
      <div className="document-info">
        <div id="document-pie">
          {currentDocument && currentDocument.metrics ? (
            <ParentSize>
              {({ width: w, height: h }: any) => {
                return (
                  <svg height={h} width={w}>
                    <Group top={0} left={0}>
                      <Pie
                        data={currentDocument.metrics}
                        width={w}
                        height={h}
                        margin={{
                          top: 0
                        }}
                        onMouseOverArc={(data: any) => {
                          setCurrentSubValue(data.value);
                        }}
                        onMouseOutArc={() => {
                          setCurrentSubValue(null);
                        }}
                        categories={categories}
                      />
                      <Text
                        x={w / 2}
                        y={h / 2}
                        verticalAnchor="middle"
                        textAnchor="middle"
                        className="pie-amount"
                      >
                        {`${totalSpent} €`}
                      </Text>
                      {currentSubValue && (
                        <Text
                          x={w / 2}
                          y={h / 2 + 30}
                          verticalAnchor="middle"
                          textAnchor="middle"
                          className="pie-sub-amount"
                        >
                          {`${Math.round(currentSubValue)} €`}
                        </Text>
                      )}
                      {currentSubValue && (
                        <Text
                          x={w / 2}
                          y={h / 2 + 55}
                          verticalAnchor="middle"
                          textAnchor="middle"
                          className="pie-percentage"
                        >
                          {`${Math.round(
                            (currentSubValue / totalSpent) * 100
                          )} %`}
                        </Text>
                      )}
                    </Group>
                  </svg>
                );
              }}
            </ParentSize>
          ) : null}
        </div>
        <div className="document-spendings">
          {currentDocument && currentDocument.spendingEntries ? (
            <div>
              <FilteringTools
                query={query}
                setQuery={setQuery}
                sortBy={sortBy}
                setSortBy={setSortBy}
              />
              {currentDocument.spendingEntries
                .filter((entry: any) => {
                  return query && query.length > 1
                    ? entry.description
                        .toLowerCase()
                        .indexOf(query.toLowerCase()) > -1 ||
                        entry.supplier
                          .toLowerCase()
                          .indexOf(query.toLowerCase()) > -1
                    : true;
                })
                .sort((a: any, b: any) => {
                  const d1 = new Date(a.date);
                  const d2 = new Date(b.date);
                  switch (sortBy) {
                    case "date ascending":
                      return d1.getTime() - d2.getTime();
                    case "date descending":
                      return d2.getTime() - d1.getTime();
                    case "price ascending":
                      return a.amount - b.amount;
                    case "price descending":
                      return b.amount - a.amount;
                    default:
                      return d1.getTime() - d2.getTime();
                  }
                })
                .map((entry: any, index: number) => {
                  return (
                    <Spending
                      key={index}
                      spending={entry}
                      color={categories[entry.category].color}
                    />
                  );
                })}
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default DocumentsViewer;
