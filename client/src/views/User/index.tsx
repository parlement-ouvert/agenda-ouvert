import {
  Button,
  Classes,
  Collapse,
  Drawer,
  FileInput,
  Icon,
  Intent,
  Position,
  ProgressBar,
  Spinner,
  Navbar as BPNavbar,
  IToastProps
} from "@blueprintjs/core";
import axios from "axios";
import * as React from "react";
import { Query, Mutation } from "react-apollo";
import { useApolloClient } from "react-apollo-hooks";
import useReactRouter from "use-react-router";

import Agenda from "./components/Item/Agenda";
import Document from "./components/Item/Document";
import Navbar from "../../components/Navbar";
import Toaster from "../../components/Toaster";

const agendasQuery = require("../../graphql/queries/agendas.graphql");
const documentsQuery = require("../../graphql/queries/documents.graphql");
const user = require("../../graphql/queries/user.graphql");
const addAgendaMutation = require("../../graphql/mutations/addAgenda.graphql");
const deleteAgendaMutation = require("../../graphql/mutations/deleteAgenda.graphql");
const deleteDocumentMutation = require("../../graphql/mutations/deleteDocument.graphql");

import "./style.scss";

const User = () => {
  const [agendas, setAgendas] = React.useReducer((agendas, { type, value }) => {
    switch (type) {
      case "add":
        return [value, ...agendas];
      case "init":
        return [...value];
      case "remove":
        return agendas.filter((_: any, index: number) => index !== value);
      default:
        return agendas;
    }
  }, []);

  const [documents, setDocuments] = React.useReducer(
    (documents, { type, value }) => {
      switch (type) {
        case "add":
          return [value, ...documents];
        case "init":
          return [...value];
        case "remove":
          return documents.filter((_: any, index: number) => index !== value);
        default:
          return documents;
      }
    },
    []
  );

  const [document, setDocument] = React.useState<File>();

  const client = useApolloClient();
  const { history } = useReactRouter();

  React.useEffect(() => {
    client
      .query({ query: agendasQuery })
      .then((response: any) => {
        setAgendas({ type: "init", value: response.data.agendas });
      })
      .catch((error: any) => {
        console.log(error);
        throw new Error();
      });
    client
      .query({ query: documentsQuery })
      .then((response: any) => {
        setDocuments({ type: "init", value: response.data.documents });
      })
      .catch((error: any) => {
        console.log(error);
        throw new Error();
      });
  }, [setAgendas, setDocuments]);

  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [isOpenGoogleAgenda, setGoogleAgenda] = React.useState(false);
  const [isOpenOutlookAgenda, setOutlookAgenda] = React.useState(false);
  const [isOpenNextcloud, setNextcloud] = React.useState(false);

  const handleClose = () => {
    setDrawerOpen(false);
  };

  const handleOpen = () => {
    setDrawerOpen(true);
  };

  const onFormSubmit = (e: any) => {
    e.preventDefault();
    fileUpload(document);
  };

  const renderProgressToaster = (amount: number): IToastProps => {
    return {
      icon: "cloud-upload",
      message: (
        <ProgressBar
          intent={amount < 100 ? Intent.PRIMARY : Intent.SUCCESS}
          value={amount / 100}
        />
      ),
      onDismiss: () => {},
      timeout: amount < 100 ? 0 : 2000
    };
  };

  const fileUpload = async (file: any) => {
    const formData = new FormData();
    formData.append("afm", file, file.name);

    const CancelToken = axios.CancelToken;

    const token = localStorage.getItem(process.env.AUTH_TOKEN!);

    try {
      const uploadToasterKey = Toaster.show(renderProgressToaster(0));
      const response = await axios.request({
        cancelToken: new CancelToken((c: any) => {}),
        data: formData,
        headers: {
          Authorization: token ? `Bearer ${token}` : ""
        },
        method: "post",
        url: `${process.env.HTTP_BACKEND_URL}/upload`,
        onUploadProgress: progress => {
          Toaster.show(
            renderProgressToaster((progress.loaded / progress.total) * 100),
            uploadToasterKey
          );
        }
      });
      // Create new source in Graphql
      if (response.data.success) {
        Toaster.show({
          icon: "saved",
          message: response.data.message,
          intent: Intent.SUCCESS,
          timeout: 2000
        });

        setDocuments({
          type: "add",
          value: response.data.document
        });
      } else {
        console.log(response.data.success);
        console.log(response.data.message);
      }
    } catch (response_1) {
      console.log("Annulé");
    }
  };

  return (
    <div>
      <Drawer
        icon="info-sign"
        onClose={handleClose}
        title="Ajouter un agenda"
        autoFocus={true}
        canEscapeKeyClose={true}
        canOutsideClickClose={true}
        enforceFocus={true}
        hasBackdrop={true}
        isOpen={drawerOpen}
        position={Position.RIGHT}
        size={undefined}
        usePortal={true}
      >
        <div className={Classes.DRAWER_BODY}>
          <div className={Classes.DIALOG_BODY}>
            <p>
              À travers cette application, il est possible de rendre tout ou
              partie de vos agendas publics. Voici un guide pour publier vos
              agendas, en fonction de la plateforme avec laquelle vous
              travaillez :
            </p>
            <ul>
              <li
                onClick={() => {
                  setGoogleAgenda(!isOpenGoogleAgenda);
                }}
              >
                <Icon
                  icon={isOpenGoogleAgenda ? "chevron-down" : "chevron-right"}
                />
                Google Agenda
              </li>
              <Collapse isOpen={isOpenGoogleAgenda}>
                <ol>
                  <li>Sur un ordinateur, ouvrez Google Agenda.</li>
                  <li>
                    En haut à droite, cliquez sur{" "}
                    <strong>
                      Paramètres <Icon icon="cog" />
                    </strong>{" "}
                    puis <strong>Paramètres.</strong>
                  </li>
                  <li>
                    Dans <strong>Paramètres de mes agendas</strong>, cliquez sur
                    le nom de l'agenda que vous souhaitez partager.
                  </li>
                  <li>
                    Dans la catégorie <strong>Autorisations d'accès</strong>,
                    cochez <strong>Rendre disponible publiquement</strong>.
                  </li>
                  <li>
                    Si vous ne souhaitez pas que les internautes puissent
                    consulter le détail de vos événements, sélectionnez Afficher
                    uniquement les infos de disponibilité (masquer les détails).
                  </li>
                  <li>
                    Dans la sous-section <strong>Intégrer l'agenda</strong>,
                    copiez l'<strong>Adresse publique au format iCal</strong>
                    et collez là ici pour publier votre agenda.
                  </li>
                </ol>
              </Collapse>
              <li
                onClick={() => {
                  setOutlookAgenda(!isOpenOutlookAgenda);
                }}
              >
                <Icon
                  icon={isOpenOutlookAgenda ? "chevron-down" : "chevron-right"}
                />
                Outlook Assemblée Nationale
              </li>
              <Collapse isOpen={isOpenOutlookAgenda}>
                En cours de rédaction.
              </Collapse>
              <li
                onClick={() => {
                  setNextcloud(!isOpenNextcloud);
                }}
              >
                <Icon
                  icon={isOpenNextcloud ? "chevron-down" : "chevron-right"}
                />
                NextCloud
              </li>
              <Collapse isOpen={isOpenNextcloud}>
                En cours de rédaction.
              </Collapse>
            </ul>
          </div>
        </div>
        <div className={Classes.DRAWER_FOOTER}>Footer</div>
      </Drawer>
      <Navbar
        rightComponent={
          <Query query={user}>
            {({ loading, data }: any) => {
              return loading ? (
                <Spinner size={20} />
              ) : (
                <BPNavbar.Group>
                  <div>{data.user.name}</div>
                  <BPNavbar.Divider />
                  <Button
                    icon={"log-out"}
                    minimal
                    onClick={() => {
                      localStorage.removeItem(process.env.AUTH_TOKEN as any);
                      history.push("/register");
                    }}
                  >
                    Se déconnecter
                  </Button>
                </BPNavbar.Group>
              );
            }}
          </Query>
        }
      />
      <div id="app-container">
        <div id="user-container">
          <h1>Mes AFM</h1>
          <form onSubmit={onFormSubmit}>
            <div className="commands">
              <Button intent="primary" type="submit" icon="upload" large>
                Envoyer
              </Button>
              <FileInput
                fill
                large
                inputProps={{
                  onChange: (event: React.FormEvent<HTMLInputElement>) => {
                    const target = event.target as any;
                    setDocument(target.files[0]);
                  },
                  name: "afm"
                }}
                text={
                  document ? (
                    document.name
                  ) : (
                    <p className="disabled-text">Importer un document...</p>
                  )
                }
              />
            </div>
          </form>
          <div className="afm-list">
            {documents.map((afm: any, index: number) => (
              <Document
                key={afm.id}
                deleteLoading={false}
                deleteAgendaCallback={() => {
                  client
                    .mutate({
                      mutation: deleteDocumentMutation,
                      variables: { id: afm.id }
                    })
                    .then(response => {
                      Toaster.show({
                        icon: "trash",
                        message: "Le document a bien été supprimé",
                        intent: Intent.SUCCESS
                      });

                      setDocuments({
                        type: "remove",
                        value: index
                      });
                    })
                    .catch(error => {
                      console.log(error);
                      throw new Error();
                    });
                }}
                id={afm.id}
                name={afm.name}
              />
            ))}
          </div>

          <h1>Mes agendas publics</h1>
          <div className="commands">
            <Mutation
              mutation={addAgendaMutation}
              onCompleted={(data: any) => {
                setAgendas({ type: "add", value: data.addAgenda });
              }}
            >
              {(addAgenda: any, { loading }: any) => {
                return (
                  <Button
                    loading={loading}
                    icon="plus"
                    id="add-agenda"
                    intent="primary"
                    large
                    onClick={() => {
                      addAgenda();
                    }}
                  >
                    Ajouter un agenda
                  </Button>
                );
              }}
            </Mutation>
            <Button large minimal icon="help" onClick={handleOpen} />
          </div>
          <div className="agenda-list">
            {agendas.map((agenda: any, index: number) => (
              <Mutation
                mutation={deleteAgendaMutation}
                key={agenda.id}
                onCompleted={() => {
                  setAgendas({ type: "remove", value: index });
                }}
              >
                {(deleteAgenda: any, { loading }: any) => {
                  return (
                    <Agenda
                      key={agenda.id}
                      deleteLoading={loading}
                      deleteAgendaCallback={() => {
                        deleteAgenda({
                          variables: {
                            id: agenda.id
                          }
                        });

                        Toaster.show({
                          icon: "trash",
                          message: "L'agenda a bien été supprimé",
                          intent: Intent.SUCCESS
                        });
                      }}
                      id={agenda.id}
                      name={agenda.name}
                      url={agenda.url}
                    />
                  );
                }}
              </Mutation>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default User;
