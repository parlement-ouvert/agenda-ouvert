import { Button, Colors, EditableText, Icon, Spinner } from "@blueprintjs/core";
import * as React from "react";
import { Mutation } from "react-apollo";

const updateAgendaMutation = require("../../../../../graphql/mutations/updateAgenda.graphql");

import "../style.scss";

interface IProps {
  deleteAgendaCallback: () => void;
  deleteLoading: boolean;
  id: string;
  name: string;
  url: string;
}

const Agenda = ({
  deleteAgendaCallback,
  deleteLoading,
  id,
  name,
  url
}: IProps) => {
  const [cname, setName] = React.useState(name);
  const [curl, setUrl] = React.useState(url);

  return (
    <Mutation mutation={updateAgendaMutation}>
      {(updateAgenda: any, { data, loading, error }: any) => {
        return (
          <div className="item">
            <div className="card">
              <div className={`card-state ${loading ? "loading" : ""}`}>
                <Spinner size={15} />
              </div>
              <div className="property title">
                <Icon icon="calendar" color={Colors.GRAY3} />
                <h2>
                  <EditableText
                    value={cname}
                    onChange={(value: string) => {
                      setName(value);
                      updateAgenda({
                        variables: {
                          id,
                          data: {
                            name: value
                          }
                        }
                      });
                    }}
                  />
                </h2>
              </div>
              <div className="property">
                <Icon icon="link" color={Colors.GRAY3} />
                <EditableText
                  value={curl}
                  onChange={(value: string) => {
                    setUrl(value);
                    updateAgenda({
                      variables: {
                        id,
                        data: {
                          url: value
                        }
                      }
                    });
                  }}
                />
              </div>
            </div>
            <div className="operations">
              <Button
                loading={deleteLoading}
                minimal
                icon="trash"
                intent="danger"
                onClick={deleteAgendaCallback}
              />
            </div>
          </div>
        );
      }}
    </Mutation>
  );
};

export default Agenda;
