import { Button, Colors, EditableText, Icon, Spinner } from "@blueprintjs/core";
import * as React from "react";
import { Mutation } from "react-apollo";

const updateAgendaMutation = require("../../../../../graphql/mutations/updateAgenda.graphql");

import "../style.scss";

interface IProps {
  deleteAgendaCallback: () => void;
  deleteLoading: boolean;
  id: string;
  name: string;
}

const Document = ({
  deleteAgendaCallback,
  deleteLoading,
  id,
  name
}: IProps) => {
  const [cname, setName] = React.useState(name);

  return (
    <div className="item">
      <div className="card">
        <div className="property title">
          <Icon icon="document" color={Colors.GRAY3} />
          <h2>{cname}</h2>
        </div>
      </div>
      <div className="operations">
        <Button
          loading={deleteLoading}
          minimal
          icon="trash"
          intent="danger"
          onClick={deleteAgendaCallback}
        />
      </div>
    </div>
  );
};

export default Document;
