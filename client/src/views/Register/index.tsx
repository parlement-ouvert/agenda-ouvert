import { InputGroup } from "@blueprintjs/core";
import * as React from "react";

import Navbar from "../../components/Navbar";
import Signup from "./components/Signup";
import Login from "./components/Login";

import "./style.scss";

const Register = () => {
  return (
    <div>
      <Navbar />
      <div id="register-container">
        <Signup />
        <Login />
      </div>
    </div>
  );
};

export default Register;
