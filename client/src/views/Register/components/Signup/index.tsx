import { Button, FormGroup, InputGroup } from "@blueprintjs/core";
import * as React from "react";
import { Mutation } from "react-apollo";
import { useApolloClient } from "react-apollo-hooks";
import useReactRouter from "use-react-router";

import "./style.scss";

const signup = require("../../../../graphql/mutations/signup.graphql");
const isAuthenticated = require("../../../../graphql/queries/isAuthenticated.graphql");

const Signup = () => {
  const [mail, setMail] = React.useState("");
  const { history } = useReactRouter();
  const client = useApolloClient();

  React.useEffect(() => {
    client
      .query({
        query: isAuthenticated,
        // This query should not use the cache,
        // or else users can't log in and out.
        fetchPolicy: "network-only"
      })
      .then((response: any) => {
        if (response.data.isAuthenticated) {
          history.push("/user");
        }
      })
      .catch((error: any) => {
        console.log(error);
        throw new Error();
      });
  }, [client, history]);

  return (
    <div id="signup-form">
      <h2>Inscription*</h2>
      <Mutation
        mutation={signup}
        onCompleted={(data: any) => {
          if (data.signup.token) {
            const token = data.signup.token;
            const { id, mail } = data.signup.user;

            localStorage.setItem(process.env.AUTH_TOKEN!, token);
            history.push("/user");
          }
        }}
        onError={(error: any) => {
          console.log(error);
          throw new Error();
        }}
      >
        {(signup: any, { data, loading }: any) => {
          return (
            <form
              onSubmit={(e: any) => {
                e.preventDefault();
                signup({
                  variables: {
                    mail
                  }
                });
              }}
            >
              <FormGroup label="Adresse mail">
                <InputGroup
                  large
                  leftIcon="inbox"
                  placeholder="Adresse mail"
                  value={mail}
                  onChange={(event: React.FormEvent<HTMLElement>) => {
                    const target = event.target as HTMLInputElement;
                    setMail(target.value);
                  }}
                />
                <Button
                  disabled={!mail.endsWith("@assemblee-nationale.fr")}
                  intent="primary"
                  large
                  type={"submit"}
                >
                  S'inscrire
                </Button>
              </FormGroup>
              <p>
                *Une adresse <code>assemblee-nationale.fr</code> est nécessaire.
              </p>
            </form>
          );
        }}
      </Mutation>
    </div>
  );
};

export default Signup;
