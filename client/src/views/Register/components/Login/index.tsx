import { Button, FormGroup, Label, InputGroup } from "@blueprintjs/core";
import * as React from "react";
import { Mutation } from "react-apollo";
import useReactRouter from "use-react-router";

import "./style.scss";

const login = require("../../../../graphql/mutations/login.graphql");

const Signin = () => {
  const [mail, setMail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const { history } = useReactRouter();

  return (
    <div id="login-form">
      <h2>Connexion</h2>
      <Mutation
        mutation={login}
        onCompleted={(data: any) => {
          if (data.login.token) {
            const token = data.login.token;
            const { id, mail } = data.login.user;
            localStorage.setItem(process.env.AUTH_TOKEN!, token);
            history.push("/user");
          }
        }}
        onError={(error: any) => {
          console.log(error);
          throw new Error();
        }}
      >
        {(login: any, { data, loading }: any) => {
          return (
            <form
              onSubmit={(e: any) => {
                e.preventDefault();
                login({
                  variables: {
                    mail,
                    password
                  }
                });
              }}
            >
              <FormGroup labelFor="text-input">
                <Label>Adresse mail</Label>
                <InputGroup
                  large
                  leftIcon="inbox"
                  placeholder="Adresse mail"
                  value={mail}
                  onChange={(event: React.FormEvent<HTMLElement>) => {
                    const target = event.target as any;
                    setMail(target.value);
                  }}
                />
                <Label>Mot de Passe</Label>
                <InputGroup
                  large
                  leftIcon="lock"
                  placeholder="Mot de passe"
                  value={password}
                  onChange={(event: React.FormEvent<HTMLElement>) => {
                    const target = event.target as any;
                    setPassword(target.value);
                  }}
                  type="password"
                />
                <Button
                  disabled={!password || !mail}
                  intent="primary"
                  large
                  type={"submit"}
                >
                  Se connecter
                </Button>
              </FormGroup>
            </form>
          );
        }}
      </Mutation>
    </div>
  );
};

export default Signin;
