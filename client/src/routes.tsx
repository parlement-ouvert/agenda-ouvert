import * as React from "react";
import { Route } from "react-router";
import { BrowserRouter, Switch } from "react-router-dom";

import MainView from "./views/Main";
import RegisterView from "./views/Register";
import UserView from "./views/User";

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={MainView} />
      <Route path="/register" component={RegisterView} />
      <Route path="/user" component={UserView} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
