# Agenda Ouvert

Un projet du [Bureau Ouvert](https://parlement-ouvert.fr) pour dôter les parlementaires d'un outil leur permettant d'ouvrir leur agenda.
