import * as fs from "fs";
import { GraphQLServer } from "graphql-yoga";
import * as multer from "multer";
import * as xlsx from "xlsx";
import "graphql-import-node";

import * as Schema from "./schema.graphql";

import { Prisma as PrismaClient } from "./generated/prisma-client";
import { Prisma as PrismaBinding } from "./generated/prisma-binding";
import resolvers from "./resolvers";
import { getUserId } from "./utils";
import { mutations } from "./resolvers/Mutation/main";
import { whileStatement } from "@babel/types";

const endpoint =
  process.env.NODE_ENV == "docker"
    ? "http://prisma:4466/agenda-ouvert"
    : "http://localhost:4466/agenda-ouvert";

const DOCUMENTS_DEST = `${process.env.STATIC_FILES_DIR}/documents`;

const server = new GraphQLServer({
  typeDefs: Schema,
  // Resolvers have different types in various dependencies.
  // A quick workaround consists in any-casting resolvers.
  // This issue can be tracked here:
  // https://github.com/prisma/graphqlgen/issues/15#issuecomment-461024244
  resolvers: resolvers as any,
  resolverValidationOptions: { requireResolversForResolveType: false },
  context: request => ({
    ...request,
    binding: new PrismaBinding({
      endpoint,
      debug: true
    }),
    client: new PrismaClient({
      endpoint,
      debug: true
    })
  })
});

server.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, `${DOCUMENTS_DEST}/`);
  }
});

var upload = multer({
  limits: {
    // Maximum file size in bytes (here, 10Mo)
    fileSize: 10 * 1024 * 1024
  },
  storage: storage
});

server.post("/upload", upload.single("afm"), async function(req, res, next) {
  console.log(
    req.file && req.file.originalname
      ? `Uploading ${req.file.originalname}...`
      : "No File Uploaded"
  );

  //TODO: accept xlsx, xls, odt files only

  const context = server.context();

  const id = getUserId(context, req.get("Authorization"));

  const document = await context.client.createDocument({
    filename: req.file.filename,
    name: req.file.originalname,
    user: { connect: { id } },
    type: "AFM"
  });

  // Parse metrics from loaded file
  const workbook = xlsx.readFile(req.file.path);
  const sheet = workbook.Sheets["Total"];

  const cells = [
    { address: "A3", type: "PERM" },
    { address: "C3", type: "MOVE" },
    { address: "E3", type: "MEAL" },
    { address: "G3", type: "LEARN" },
    { address: "I3", type: "COM" },
    { address: "K3", type: "REC" },
    { address: "M3", type: "PPL" },
    { address: "O3", type: "END" },
    { address: "Q3", type: "MISC" },
    { address: "S3", type: "UNJUST" }
  ];
  const cellValues = cells.map(cell =>
    sheet[cell.address] ? sheet[cell.address].v : undefined
  );

  // Write cell values to database
  await cellValues.forEach(async (cellValue, index) => {
    await context.client.createMetric({
      value: cellValue,
      document: { connect: { id: document.id } },
      type: cells[index].type
    });
  });

  // Parse all inputs from loaded file
  const sheetNames = [
    "01. Perm.",
    "02. Déplac.",
    "03. Héberg.",
    "04. Format.",
    "05. Com. et Doc.",
    "06. Récp, et représ,",
    "07. perso. et serv.",
    "08.fin mandat",
    "09. Divers",
    "10. Dépenses sans justif."
  ];

  const getCellValue = (cell: any) => {
    return cell && cell.v ? cell.v : undefined;
  };

  const parseDateCell = (cell: any) => {
    if (cell && cell.w) {
      const dateInfo = cell.w.split("/").reverse();
      const date = new Date(+`20${dateInfo[0]}`, dateInfo[2], dateInfo[1]);
      return date.toISOString().slice(0, 10);
    }
    return undefined;
  };

  sheetNames.forEach(async (sheetName: string, index: number) => {
    const sheet = workbook.Sheets[sheetName];

    let i = 3;
    while (getCellValue(sheet[`H${i}`])) {
      await context.client.createSpendingEntry({
        reference: getCellValue(sheet[`A${i}`]),
        designation: getCellValue(sheet[`D${i}`]),
        date: parseDateCell(sheet[`E${i}`]),
        paymentMode: getCellValue(sheet[`F${i}`]),
        supplier: getCellValue(sheet[`G${i}`]),
        amount: getCellValue(sheet[`H${i}`]),
        description: getCellValue(sheet[`I${i}`]),
        observation: getCellValue(sheet[`J${i}`]),
        document: { connect: { id: document.id } },
        category: cells[index].type
      });

      i++;
    }
  });

  // Response message
  res.send({
    message:
      req.file && req.file.filename
        ? "Document transféré"
        : "Une erreur est survenue",
    success: req.file && req.file.filename,
    document: document
  });
});

const serverOptions = {
  port: process.env.SERVER_PORT
};

server.start(serverOptions, ({ port }) =>
  console.log(`🚀 Server is running on http://localhost:${port}`)
);
