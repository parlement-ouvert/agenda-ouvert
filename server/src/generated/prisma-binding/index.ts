import { GraphQLResolveInfo, GraphQLSchema } from "graphql";
import { IResolvers } from "graphql-tools/dist/Interfaces";
import { Options } from "graphql-binding";
import { makePrismaBindingClass, BasePrismaOptions } from "prisma-binding";

export interface Query {
  users: <T = Array<User | null>>(
    args: {
      where?: UserWhereInput | null;
      orderBy?: UserOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  agendas: <T = Array<Agenda | null>>(
    args: {
      where?: AgendaWhereInput | null;
      orderBy?: AgendaOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  documents: <T = Array<Document | null>>(
    args: {
      where?: DocumentWhereInput | null;
      orderBy?: DocumentOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  spendingEntries: <T = Array<SpendingEntry | null>>(
    args: {
      where?: SpendingEntryWhereInput | null;
      orderBy?: SpendingEntryOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  metrics: <T = Array<Metric | null>>(
    args: {
      where?: MetricWhereInput | null;
      orderBy?: MetricOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  user: <T = User | null>(
    args: { where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  agenda: <T = Agenda | null>(
    args: { where: AgendaWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  document: <T = Document | null>(
    args: { where: DocumentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  spendingEntry: <T = SpendingEntry | null>(
    args: { where: SpendingEntryWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  metric: <T = Metric | null>(
    args: { where: MetricWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  usersConnection: <T = UserConnection>(
    args: {
      where?: UserWhereInput | null;
      orderBy?: UserOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  agendasConnection: <T = AgendaConnection>(
    args: {
      where?: AgendaWhereInput | null;
      orderBy?: AgendaOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  documentsConnection: <T = DocumentConnection>(
    args: {
      where?: DocumentWhereInput | null;
      orderBy?: DocumentOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  spendingEntriesConnection: <T = SpendingEntryConnection>(
    args: {
      where?: SpendingEntryWhereInput | null;
      orderBy?: SpendingEntryOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  metricsConnection: <T = MetricConnection>(
    args: {
      where?: MetricWhereInput | null;
      orderBy?: MetricOrderByInput | null;
      skip?: Int | null;
      after?: String | null;
      before?: String | null;
      first?: Int | null;
      last?: Int | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  node: <T = Node | null>(
    args: { id: ID_Output },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
}

export interface Mutation {
  createUser: <T = User>(
    args: { data: UserCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  createAgenda: <T = Agenda>(
    args: { data: AgendaCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  createDocument: <T = Document>(
    args: { data: DocumentCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  createSpendingEntry: <T = SpendingEntry>(
    args: { data: SpendingEntryCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  createMetric: <T = Metric>(
    args: { data: MetricCreateInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  updateUser: <T = User | null>(
    args: { data: UserUpdateInput; where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  updateAgenda: <T = Agenda | null>(
    args: { data: AgendaUpdateInput; where: AgendaWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  updateDocument: <T = Document | null>(
    args: { data: DocumentUpdateInput; where: DocumentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  updateSpendingEntry: <T = SpendingEntry | null>(
    args: {
      data: SpendingEntryUpdateInput;
      where: SpendingEntryWhereUniqueInput;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  updateMetric: <T = Metric | null>(
    args: { data: MetricUpdateInput; where: MetricWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  deleteUser: <T = User | null>(
    args: { where: UserWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  deleteAgenda: <T = Agenda | null>(
    args: { where: AgendaWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  deleteDocument: <T = Document | null>(
    args: { where: DocumentWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  deleteSpendingEntry: <T = SpendingEntry | null>(
    args: { where: SpendingEntryWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  deleteMetric: <T = Metric | null>(
    args: { where: MetricWhereUniqueInput },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T | null>;
  upsertUser: <T = User>(
    args: {
      where: UserWhereUniqueInput;
      create: UserCreateInput;
      update: UserUpdateInput;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  upsertAgenda: <T = Agenda>(
    args: {
      where: AgendaWhereUniqueInput;
      create: AgendaCreateInput;
      update: AgendaUpdateInput;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  upsertDocument: <T = Document>(
    args: {
      where: DocumentWhereUniqueInput;
      create: DocumentCreateInput;
      update: DocumentUpdateInput;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  upsertSpendingEntry: <T = SpendingEntry>(
    args: {
      where: SpendingEntryWhereUniqueInput;
      create: SpendingEntryCreateInput;
      update: SpendingEntryUpdateInput;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  upsertMetric: <T = Metric>(
    args: {
      where: MetricWhereUniqueInput;
      create: MetricCreateInput;
      update: MetricUpdateInput;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  updateManyUsers: <T = BatchPayload>(
    args: { data: UserUpdateManyMutationInput; where?: UserWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  updateManyAgendas: <T = BatchPayload>(
    args: {
      data: AgendaUpdateManyMutationInput;
      where?: AgendaWhereInput | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  updateManyDocuments: <T = BatchPayload>(
    args: {
      data: DocumentUpdateManyMutationInput;
      where?: DocumentWhereInput | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  updateManySpendingEntries: <T = BatchPayload>(
    args: {
      data: SpendingEntryUpdateManyMutationInput;
      where?: SpendingEntryWhereInput | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  updateManyMetrics: <T = BatchPayload>(
    args: {
      data: MetricUpdateManyMutationInput;
      where?: MetricWhereInput | null;
    },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  deleteManyUsers: <T = BatchPayload>(
    args: { where?: UserWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  deleteManyAgendas: <T = BatchPayload>(
    args: { where?: AgendaWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  deleteManyDocuments: <T = BatchPayload>(
    args: { where?: DocumentWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  deleteManySpendingEntries: <T = BatchPayload>(
    args: { where?: SpendingEntryWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
  deleteManyMetrics: <T = BatchPayload>(
    args: { where?: MetricWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<T>;
}

export interface Subscription {
  user: <T = UserSubscriptionPayload | null>(
    args: { where?: UserSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>;
  agenda: <T = AgendaSubscriptionPayload | null>(
    args: { where?: AgendaSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>;
  document: <T = DocumentSubscriptionPayload | null>(
    args: { where?: DocumentSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>;
  spendingEntry: <T = SpendingEntrySubscriptionPayload | null>(
    args: { where?: SpendingEntrySubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>;
  metric: <T = MetricSubscriptionPayload | null>(
    args: { where?: MetricSubscriptionWhereInput | null },
    info?: GraphQLResolveInfo | string,
    options?: Options
  ) => Promise<AsyncIterator<T | null>>;
}

export interface Exists {
  User: (where?: UserWhereInput) => Promise<boolean>;
  Agenda: (where?: AgendaWhereInput) => Promise<boolean>;
  Document: (where?: DocumentWhereInput) => Promise<boolean>;
  SpendingEntry: (where?: SpendingEntryWhereInput) => Promise<boolean>;
  Metric: (where?: MetricWhereInput) => Promise<boolean>;
}

export interface Prisma {
  query: Query;
  mutation: Mutation;
  subscription: Subscription;
  exists: Exists;
  request: <T = any>(
    query: string,
    variables?: { [key: string]: any }
  ) => Promise<T>;
  delegate(
    operation: "query" | "mutation",
    fieldName: string,
    args: {
      [key: string]: any;
    },
    infoOrQuery?: GraphQLResolveInfo | string,
    options?: Options
  ): Promise<any>;
  delegateSubscription(
    fieldName: string,
    args?: {
      [key: string]: any;
    },
    infoOrQuery?: GraphQLResolveInfo | string,
    options?: Options
  ): Promise<AsyncIterator<any>>;
  getAbstractResolvers(filterSchema?: GraphQLSchema | string): IResolvers;
}

export interface BindingConstructor<T> {
  new (options: BasePrismaOptions): T;
}
/**
 * Type Defs
 */

const typeDefs = `type Agenda implements Node {
  id: ID!
  name: String
  url: String
  user: User!
  updatedAt: DateTime!
  createdAt: DateTime!
}

"""A connection to a list of items."""
type AgendaConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [AgendaEdge]!
  aggregate: AggregateAgenda!
}

input AgendaCreateInput {
  id: ID
  name: String
  url: String
  user: UserCreateOneWithoutAgendasInput!
}

input AgendaCreateManyWithoutUserInput {
  create: [AgendaCreateWithoutUserInput!]
  connect: [AgendaWhereUniqueInput!]
}

input AgendaCreateWithoutUserInput {
  id: ID
  name: String
  url: String
}

"""An edge in a connection."""
type AgendaEdge {
  """The item at the end of the edge."""
  node: Agenda!

  """A cursor for use in pagination."""
  cursor: String!
}

enum AgendaOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  url_ASC
  url_DESC
  updatedAt_ASC
  updatedAt_DESC
  createdAt_ASC
  createdAt_DESC
}

type AgendaPreviousValues {
  id: ID!
  name: String
  url: String
  updatedAt: DateTime!
  createdAt: DateTime!
}

input AgendaScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [AgendaScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [AgendaScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AgendaScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  url: String

  """All values that are not equal to given value."""
  url_not: String

  """All values that are contained in given list."""
  url_in: [String!]

  """All values that are not contained in given list."""
  url_not_in: [String!]

  """All values less than the given value."""
  url_lt: String

  """All values less than or equal the given value."""
  url_lte: String

  """All values greater than the given value."""
  url_gt: String

  """All values greater than or equal the given value."""
  url_gte: String

  """All values containing the given string."""
  url_contains: String

  """All values not containing the given string."""
  url_not_contains: String

  """All values starting with the given string."""
  url_starts_with: String

  """All values not starting with the given string."""
  url_not_starts_with: String

  """All values ending with the given string."""
  url_ends_with: String

  """All values not ending with the given string."""
  url_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
}

type AgendaSubscriptionPayload {
  mutation: MutationType!
  node: Agenda
  updatedFields: [String!]
  previousValues: AgendaPreviousValues
}

input AgendaSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [AgendaSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [AgendaSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AgendaSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: AgendaWhereInput
}

input AgendaUpdateInput {
  name: String
  url: String
  user: UserUpdateOneRequiredWithoutAgendasInput
}

input AgendaUpdateManyDataInput {
  name: String
  url: String
}

input AgendaUpdateManyMutationInput {
  name: String
  url: String
}

input AgendaUpdateManyWithoutUserInput {
  create: [AgendaCreateWithoutUserInput!]
  connect: [AgendaWhereUniqueInput!]
  set: [AgendaWhereUniqueInput!]
  disconnect: [AgendaWhereUniqueInput!]
  delete: [AgendaWhereUniqueInput!]
  update: [AgendaUpdateWithWhereUniqueWithoutUserInput!]
  updateMany: [AgendaUpdateManyWithWhereNestedInput!]
  deleteMany: [AgendaScalarWhereInput!]
  upsert: [AgendaUpsertWithWhereUniqueWithoutUserInput!]
}

input AgendaUpdateManyWithWhereNestedInput {
  where: AgendaScalarWhereInput!
  data: AgendaUpdateManyDataInput!
}

input AgendaUpdateWithoutUserDataInput {
  name: String
  url: String
}

input AgendaUpdateWithWhereUniqueWithoutUserInput {
  where: AgendaWhereUniqueInput!
  data: AgendaUpdateWithoutUserDataInput!
}

input AgendaUpsertWithWhereUniqueWithoutUserInput {
  where: AgendaWhereUniqueInput!
  update: AgendaUpdateWithoutUserDataInput!
  create: AgendaCreateWithoutUserInput!
}

input AgendaWhereInput {
  """Logical AND on all given filters."""
  AND: [AgendaWhereInput!]

  """Logical OR on all given filters."""
  OR: [AgendaWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [AgendaWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  url: String

  """All values that are not equal to given value."""
  url_not: String

  """All values that are contained in given list."""
  url_in: [String!]

  """All values that are not contained in given list."""
  url_not_in: [String!]

  """All values less than the given value."""
  url_lt: String

  """All values less than or equal the given value."""
  url_lte: String

  """All values greater than the given value."""
  url_gt: String

  """All values greater than or equal the given value."""
  url_gte: String

  """All values containing the given string."""
  url_contains: String

  """All values not containing the given string."""
  url_not_contains: String

  """All values starting with the given string."""
  url_starts_with: String

  """All values not starting with the given string."""
  url_not_starts_with: String

  """All values ending with the given string."""
  url_ends_with: String

  """All values not ending with the given string."""
  url_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  user: UserWhereInput
}

input AgendaWhereUniqueInput {
  id: ID
}

type AggregateAgenda {
  count: Int!
}

type AggregateDocument {
  count: Int!
}

type AggregateMetric {
  count: Int!
}

type AggregateSpendingEntry {
  count: Int!
}

type AggregateUser {
  count: Int!
}

type BatchPayload {
  """The number of nodes that have been affected by the Batch operation."""
  count: Long!
}

scalar DateTime

type Document implements Node {
  id: ID!
  name: String
  filename: String!
  user: User!
  type: DocumentType
  metrics(where: MetricWhereInput, orderBy: MetricOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Metric!]
  spendingEntries(where: SpendingEntryWhereInput, orderBy: SpendingEntryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [SpendingEntry!]
  updatedAt: DateTime!
  createdAt: DateTime!
}

"""A connection to a list of items."""
type DocumentConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [DocumentEdge]!
  aggregate: AggregateDocument!
}

input DocumentCreateInput {
  id: ID
  name: String
  filename: String!
  type: DocumentType
  user: UserCreateOneWithoutDocumentsInput!
  metrics: MetricCreateManyWithoutDocumentInput
  spendingEntries: SpendingEntryCreateManyWithoutDocumentInput
}

input DocumentCreateManyWithoutUserInput {
  create: [DocumentCreateWithoutUserInput!]
  connect: [DocumentWhereUniqueInput!]
}

input DocumentCreateOneWithoutMetricsInput {
  create: DocumentCreateWithoutMetricsInput
  connect: DocumentWhereUniqueInput
}

input DocumentCreateOneWithoutSpendingEntriesInput {
  create: DocumentCreateWithoutSpendingEntriesInput
  connect: DocumentWhereUniqueInput
}

input DocumentCreateWithoutMetricsInput {
  id: ID
  name: String
  filename: String!
  type: DocumentType
  user: UserCreateOneWithoutDocumentsInput!
  spendingEntries: SpendingEntryCreateManyWithoutDocumentInput
}

input DocumentCreateWithoutSpendingEntriesInput {
  id: ID
  name: String
  filename: String!
  type: DocumentType
  user: UserCreateOneWithoutDocumentsInput!
  metrics: MetricCreateManyWithoutDocumentInput
}

input DocumentCreateWithoutUserInput {
  id: ID
  name: String
  filename: String!
  type: DocumentType
  metrics: MetricCreateManyWithoutDocumentInput
  spendingEntries: SpendingEntryCreateManyWithoutDocumentInput
}

"""An edge in a connection."""
type DocumentEdge {
  """The item at the end of the edge."""
  node: Document!

  """A cursor for use in pagination."""
  cursor: String!
}

enum DocumentOrderByInput {
  id_ASC
  id_DESC
  name_ASC
  name_DESC
  filename_ASC
  filename_DESC
  type_ASC
  type_DESC
  updatedAt_ASC
  updatedAt_DESC
  createdAt_ASC
  createdAt_DESC
}

type DocumentPreviousValues {
  id: ID!
  name: String
  filename: String!
  type: DocumentType
  updatedAt: DateTime!
  createdAt: DateTime!
}

input DocumentScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [DocumentScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [DocumentScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [DocumentScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  filename: String

  """All values that are not equal to given value."""
  filename_not: String

  """All values that are contained in given list."""
  filename_in: [String!]

  """All values that are not contained in given list."""
  filename_not_in: [String!]

  """All values less than the given value."""
  filename_lt: String

  """All values less than or equal the given value."""
  filename_lte: String

  """All values greater than the given value."""
  filename_gt: String

  """All values greater than or equal the given value."""
  filename_gte: String

  """All values containing the given string."""
  filename_contains: String

  """All values not containing the given string."""
  filename_not_contains: String

  """All values starting with the given string."""
  filename_starts_with: String

  """All values not starting with the given string."""
  filename_not_starts_with: String

  """All values ending with the given string."""
  filename_ends_with: String

  """All values not ending with the given string."""
  filename_not_ends_with: String
  type: DocumentType

  """All values that are not equal to given value."""
  type_not: DocumentType

  """All values that are contained in given list."""
  type_in: [DocumentType!]

  """All values that are not contained in given list."""
  type_not_in: [DocumentType!]
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
}

type DocumentSubscriptionPayload {
  mutation: MutationType!
  node: Document
  updatedFields: [String!]
  previousValues: DocumentPreviousValues
}

input DocumentSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [DocumentSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [DocumentSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [DocumentSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: DocumentWhereInput
}

enum DocumentType {
  AFM
}

input DocumentUpdateInput {
  name: String
  filename: String
  type: DocumentType
  user: UserUpdateOneRequiredWithoutDocumentsInput
  metrics: MetricUpdateManyWithoutDocumentInput
  spendingEntries: SpendingEntryUpdateManyWithoutDocumentInput
}

input DocumentUpdateManyDataInput {
  name: String
  filename: String
  type: DocumentType
}

input DocumentUpdateManyMutationInput {
  name: String
  filename: String
  type: DocumentType
}

input DocumentUpdateManyWithoutUserInput {
  create: [DocumentCreateWithoutUserInput!]
  connect: [DocumentWhereUniqueInput!]
  set: [DocumentWhereUniqueInput!]
  disconnect: [DocumentWhereUniqueInput!]
  delete: [DocumentWhereUniqueInput!]
  update: [DocumentUpdateWithWhereUniqueWithoutUserInput!]
  updateMany: [DocumentUpdateManyWithWhereNestedInput!]
  deleteMany: [DocumentScalarWhereInput!]
  upsert: [DocumentUpsertWithWhereUniqueWithoutUserInput!]
}

input DocumentUpdateManyWithWhereNestedInput {
  where: DocumentScalarWhereInput!
  data: DocumentUpdateManyDataInput!
}

input DocumentUpdateOneRequiredWithoutMetricsInput {
  create: DocumentCreateWithoutMetricsInput
  connect: DocumentWhereUniqueInput
  update: DocumentUpdateWithoutMetricsDataInput
  upsert: DocumentUpsertWithoutMetricsInput
}

input DocumentUpdateOneWithoutSpendingEntriesInput {
  create: DocumentCreateWithoutSpendingEntriesInput
  connect: DocumentWhereUniqueInput
  disconnect: Boolean
  delete: Boolean
  update: DocumentUpdateWithoutSpendingEntriesDataInput
  upsert: DocumentUpsertWithoutSpendingEntriesInput
}

input DocumentUpdateWithoutMetricsDataInput {
  name: String
  filename: String
  type: DocumentType
  user: UserUpdateOneRequiredWithoutDocumentsInput
  spendingEntries: SpendingEntryUpdateManyWithoutDocumentInput
}

input DocumentUpdateWithoutSpendingEntriesDataInput {
  name: String
  filename: String
  type: DocumentType
  user: UserUpdateOneRequiredWithoutDocumentsInput
  metrics: MetricUpdateManyWithoutDocumentInput
}

input DocumentUpdateWithoutUserDataInput {
  name: String
  filename: String
  type: DocumentType
  metrics: MetricUpdateManyWithoutDocumentInput
  spendingEntries: SpendingEntryUpdateManyWithoutDocumentInput
}

input DocumentUpdateWithWhereUniqueWithoutUserInput {
  where: DocumentWhereUniqueInput!
  data: DocumentUpdateWithoutUserDataInput!
}

input DocumentUpsertWithoutMetricsInput {
  update: DocumentUpdateWithoutMetricsDataInput!
  create: DocumentCreateWithoutMetricsInput!
}

input DocumentUpsertWithoutSpendingEntriesInput {
  update: DocumentUpdateWithoutSpendingEntriesDataInput!
  create: DocumentCreateWithoutSpendingEntriesInput!
}

input DocumentUpsertWithWhereUniqueWithoutUserInput {
  where: DocumentWhereUniqueInput!
  update: DocumentUpdateWithoutUserDataInput!
  create: DocumentCreateWithoutUserInput!
}

input DocumentWhereInput {
  """Logical AND on all given filters."""
  AND: [DocumentWhereInput!]

  """Logical OR on all given filters."""
  OR: [DocumentWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [DocumentWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  filename: String

  """All values that are not equal to given value."""
  filename_not: String

  """All values that are contained in given list."""
  filename_in: [String!]

  """All values that are not contained in given list."""
  filename_not_in: [String!]

  """All values less than the given value."""
  filename_lt: String

  """All values less than or equal the given value."""
  filename_lte: String

  """All values greater than the given value."""
  filename_gt: String

  """All values greater than or equal the given value."""
  filename_gte: String

  """All values containing the given string."""
  filename_contains: String

  """All values not containing the given string."""
  filename_not_contains: String

  """All values starting with the given string."""
  filename_starts_with: String

  """All values not starting with the given string."""
  filename_not_starts_with: String

  """All values ending with the given string."""
  filename_ends_with: String

  """All values not ending with the given string."""
  filename_not_ends_with: String
  type: DocumentType

  """All values that are not equal to given value."""
  type_not: DocumentType

  """All values that are contained in given list."""
  type_in: [DocumentType!]

  """All values that are not contained in given list."""
  type_not_in: [DocumentType!]
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  user: UserWhereInput
  metrics_every: MetricWhereInput
  metrics_some: MetricWhereInput
  metrics_none: MetricWhereInput
  spendingEntries_every: SpendingEntryWhereInput
  spendingEntries_some: SpendingEntryWhereInput
  spendingEntries_none: SpendingEntryWhereInput
}

input DocumentWhereUniqueInput {
  id: ID
  filename: String
}

"""
The \`Long\` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
"""
scalar Long

type Metric implements Node {
  id: ID!
  document: Document!
  value: Float!
  type: SpendingType!
}

"""A connection to a list of items."""
type MetricConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [MetricEdge]!
  aggregate: AggregateMetric!
}

input MetricCreateInput {
  id: ID
  value: Float!
  type: SpendingType!
  document: DocumentCreateOneWithoutMetricsInput!
}

input MetricCreateManyWithoutDocumentInput {
  create: [MetricCreateWithoutDocumentInput!]
  connect: [MetricWhereUniqueInput!]
}

input MetricCreateWithoutDocumentInput {
  id: ID
  value: Float!
  type: SpendingType!
}

"""An edge in a connection."""
type MetricEdge {
  """The item at the end of the edge."""
  node: Metric!

  """A cursor for use in pagination."""
  cursor: String!
}

enum MetricOrderByInput {
  id_ASC
  id_DESC
  value_ASC
  value_DESC
  type_ASC
  type_DESC
}

type MetricPreviousValues {
  id: ID!
  value: Float!
  type: SpendingType!
}

input MetricScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [MetricScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [MetricScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [MetricScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  value: Float

  """All values that are not equal to given value."""
  value_not: Float

  """All values that are contained in given list."""
  value_in: [Float!]

  """All values that are not contained in given list."""
  value_not_in: [Float!]

  """All values less than the given value."""
  value_lt: Float

  """All values less than or equal the given value."""
  value_lte: Float

  """All values greater than the given value."""
  value_gt: Float

  """All values greater than or equal the given value."""
  value_gte: Float
  type: SpendingType

  """All values that are not equal to given value."""
  type_not: SpendingType

  """All values that are contained in given list."""
  type_in: [SpendingType!]

  """All values that are not contained in given list."""
  type_not_in: [SpendingType!]
}

type MetricSubscriptionPayload {
  mutation: MutationType!
  node: Metric
  updatedFields: [String!]
  previousValues: MetricPreviousValues
}

input MetricSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [MetricSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [MetricSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [MetricSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: MetricWhereInput
}

input MetricUpdateInput {
  value: Float
  type: SpendingType
  document: DocumentUpdateOneRequiredWithoutMetricsInput
}

input MetricUpdateManyDataInput {
  value: Float
  type: SpendingType
}

input MetricUpdateManyMutationInput {
  value: Float
  type: SpendingType
}

input MetricUpdateManyWithoutDocumentInput {
  create: [MetricCreateWithoutDocumentInput!]
  connect: [MetricWhereUniqueInput!]
  set: [MetricWhereUniqueInput!]
  disconnect: [MetricWhereUniqueInput!]
  delete: [MetricWhereUniqueInput!]
  update: [MetricUpdateWithWhereUniqueWithoutDocumentInput!]
  updateMany: [MetricUpdateManyWithWhereNestedInput!]
  deleteMany: [MetricScalarWhereInput!]
  upsert: [MetricUpsertWithWhereUniqueWithoutDocumentInput!]
}

input MetricUpdateManyWithWhereNestedInput {
  where: MetricScalarWhereInput!
  data: MetricUpdateManyDataInput!
}

input MetricUpdateWithoutDocumentDataInput {
  value: Float
  type: SpendingType
}

input MetricUpdateWithWhereUniqueWithoutDocumentInput {
  where: MetricWhereUniqueInput!
  data: MetricUpdateWithoutDocumentDataInput!
}

input MetricUpsertWithWhereUniqueWithoutDocumentInput {
  where: MetricWhereUniqueInput!
  update: MetricUpdateWithoutDocumentDataInput!
  create: MetricCreateWithoutDocumentInput!
}

input MetricWhereInput {
  """Logical AND on all given filters."""
  AND: [MetricWhereInput!]

  """Logical OR on all given filters."""
  OR: [MetricWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [MetricWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  value: Float

  """All values that are not equal to given value."""
  value_not: Float

  """All values that are contained in given list."""
  value_in: [Float!]

  """All values that are not contained in given list."""
  value_not_in: [Float!]

  """All values less than the given value."""
  value_lt: Float

  """All values less than or equal the given value."""
  value_lte: Float

  """All values greater than the given value."""
  value_gt: Float

  """All values greater than or equal the given value."""
  value_gte: Float
  type: SpendingType

  """All values that are not equal to given value."""
  type_not: SpendingType

  """All values that are contained in given list."""
  type_in: [SpendingType!]

  """All values that are not contained in given list."""
  type_not_in: [SpendingType!]
  document: DocumentWhereInput
}

input MetricWhereUniqueInput {
  id: ID
}

type Mutation {
  createUser(data: UserCreateInput!): User!
  createAgenda(data: AgendaCreateInput!): Agenda!
  createDocument(data: DocumentCreateInput!): Document!
  createSpendingEntry(data: SpendingEntryCreateInput!): SpendingEntry!
  createMetric(data: MetricCreateInput!): Metric!
  updateUser(data: UserUpdateInput!, where: UserWhereUniqueInput!): User
  updateAgenda(data: AgendaUpdateInput!, where: AgendaWhereUniqueInput!): Agenda
  updateDocument(data: DocumentUpdateInput!, where: DocumentWhereUniqueInput!): Document
  updateSpendingEntry(data: SpendingEntryUpdateInput!, where: SpendingEntryWhereUniqueInput!): SpendingEntry
  updateMetric(data: MetricUpdateInput!, where: MetricWhereUniqueInput!): Metric
  deleteUser(where: UserWhereUniqueInput!): User
  deleteAgenda(where: AgendaWhereUniqueInput!): Agenda
  deleteDocument(where: DocumentWhereUniqueInput!): Document
  deleteSpendingEntry(where: SpendingEntryWhereUniqueInput!): SpendingEntry
  deleteMetric(where: MetricWhereUniqueInput!): Metric
  upsertUser(where: UserWhereUniqueInput!, create: UserCreateInput!, update: UserUpdateInput!): User!
  upsertAgenda(where: AgendaWhereUniqueInput!, create: AgendaCreateInput!, update: AgendaUpdateInput!): Agenda!
  upsertDocument(where: DocumentWhereUniqueInput!, create: DocumentCreateInput!, update: DocumentUpdateInput!): Document!
  upsertSpendingEntry(where: SpendingEntryWhereUniqueInput!, create: SpendingEntryCreateInput!, update: SpendingEntryUpdateInput!): SpendingEntry!
  upsertMetric(where: MetricWhereUniqueInput!, create: MetricCreateInput!, update: MetricUpdateInput!): Metric!
  updateManyUsers(data: UserUpdateManyMutationInput!, where: UserWhereInput): BatchPayload!
  updateManyAgendas(data: AgendaUpdateManyMutationInput!, where: AgendaWhereInput): BatchPayload!
  updateManyDocuments(data: DocumentUpdateManyMutationInput!, where: DocumentWhereInput): BatchPayload!
  updateManySpendingEntries(data: SpendingEntryUpdateManyMutationInput!, where: SpendingEntryWhereInput): BatchPayload!
  updateManyMetrics(data: MetricUpdateManyMutationInput!, where: MetricWhereInput): BatchPayload!
  deleteManyUsers(where: UserWhereInput): BatchPayload!
  deleteManyAgendas(where: AgendaWhereInput): BatchPayload!
  deleteManyDocuments(where: DocumentWhereInput): BatchPayload!
  deleteManySpendingEntries(where: SpendingEntryWhereInput): BatchPayload!
  deleteManyMetrics(where: MetricWhereInput): BatchPayload!
}

enum MutationType {
  CREATED
  UPDATED
  DELETED
}

"""An object with an ID"""
interface Node {
  """The id of the object."""
  id: ID!
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: String

  """When paginating forwards, the cursor to continue."""
  endCursor: String
}

type Query {
  users(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [User]!
  agendas(where: AgendaWhereInput, orderBy: AgendaOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Agenda]!
  documents(where: DocumentWhereInput, orderBy: DocumentOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Document]!
  spendingEntries(where: SpendingEntryWhereInput, orderBy: SpendingEntryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [SpendingEntry]!
  metrics(where: MetricWhereInput, orderBy: MetricOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Metric]!
  user(where: UserWhereUniqueInput!): User
  agenda(where: AgendaWhereUniqueInput!): Agenda
  document(where: DocumentWhereUniqueInput!): Document
  spendingEntry(where: SpendingEntryWhereUniqueInput!): SpendingEntry
  metric(where: MetricWhereUniqueInput!): Metric
  usersConnection(where: UserWhereInput, orderBy: UserOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): UserConnection!
  agendasConnection(where: AgendaWhereInput, orderBy: AgendaOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): AgendaConnection!
  documentsConnection(where: DocumentWhereInput, orderBy: DocumentOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): DocumentConnection!
  spendingEntriesConnection(where: SpendingEntryWhereInput, orderBy: SpendingEntryOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): SpendingEntryConnection!
  metricsConnection(where: MetricWhereInput, orderBy: MetricOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): MetricConnection!

  """Fetches an object given its ID"""
  node(
    """The ID of an object"""
    id: ID!
  ): Node
}

type SpendingEntry implements Node {
  id: ID!
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  document: Document
  category: SpendingType
}

"""A connection to a list of items."""
type SpendingEntryConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [SpendingEntryEdge]!
  aggregate: AggregateSpendingEntry!
}

input SpendingEntryCreateInput {
  id: ID
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  category: SpendingType
  document: DocumentCreateOneWithoutSpendingEntriesInput
}

input SpendingEntryCreateManyWithoutDocumentInput {
  create: [SpendingEntryCreateWithoutDocumentInput!]
  connect: [SpendingEntryWhereUniqueInput!]
}

input SpendingEntryCreateWithoutDocumentInput {
  id: ID
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  category: SpendingType
}

"""An edge in a connection."""
type SpendingEntryEdge {
  """The item at the end of the edge."""
  node: SpendingEntry!

  """A cursor for use in pagination."""
  cursor: String!
}

enum SpendingEntryOrderByInput {
  id_ASC
  id_DESC
  reference_ASC
  reference_DESC
  designation_ASC
  designation_DESC
  date_ASC
  date_DESC
  paymentMode_ASC
  paymentMode_DESC
  supplier_ASC
  supplier_DESC
  amount_ASC
  amount_DESC
  description_ASC
  description_DESC
  observation_ASC
  observation_DESC
  category_ASC
  category_DESC
}

type SpendingEntryPreviousValues {
  id: ID!
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  category: SpendingType
}

input SpendingEntryScalarWhereInput {
  """Logical AND on all given filters."""
  AND: [SpendingEntryScalarWhereInput!]

  """Logical OR on all given filters."""
  OR: [SpendingEntryScalarWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [SpendingEntryScalarWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  reference: String

  """All values that are not equal to given value."""
  reference_not: String

  """All values that are contained in given list."""
  reference_in: [String!]

  """All values that are not contained in given list."""
  reference_not_in: [String!]

  """All values less than the given value."""
  reference_lt: String

  """All values less than or equal the given value."""
  reference_lte: String

  """All values greater than the given value."""
  reference_gt: String

  """All values greater than or equal the given value."""
  reference_gte: String

  """All values containing the given string."""
  reference_contains: String

  """All values not containing the given string."""
  reference_not_contains: String

  """All values starting with the given string."""
  reference_starts_with: String

  """All values not starting with the given string."""
  reference_not_starts_with: String

  """All values ending with the given string."""
  reference_ends_with: String

  """All values not ending with the given string."""
  reference_not_ends_with: String
  designation: String

  """All values that are not equal to given value."""
  designation_not: String

  """All values that are contained in given list."""
  designation_in: [String!]

  """All values that are not contained in given list."""
  designation_not_in: [String!]

  """All values less than the given value."""
  designation_lt: String

  """All values less than or equal the given value."""
  designation_lte: String

  """All values greater than the given value."""
  designation_gt: String

  """All values greater than or equal the given value."""
  designation_gte: String

  """All values containing the given string."""
  designation_contains: String

  """All values not containing the given string."""
  designation_not_contains: String

  """All values starting with the given string."""
  designation_starts_with: String

  """All values not starting with the given string."""
  designation_not_starts_with: String

  """All values ending with the given string."""
  designation_ends_with: String

  """All values not ending with the given string."""
  designation_not_ends_with: String
  date: DateTime

  """All values that are not equal to given value."""
  date_not: DateTime

  """All values that are contained in given list."""
  date_in: [DateTime!]

  """All values that are not contained in given list."""
  date_not_in: [DateTime!]

  """All values less than the given value."""
  date_lt: DateTime

  """All values less than or equal the given value."""
  date_lte: DateTime

  """All values greater than the given value."""
  date_gt: DateTime

  """All values greater than or equal the given value."""
  date_gte: DateTime
  paymentMode: String

  """All values that are not equal to given value."""
  paymentMode_not: String

  """All values that are contained in given list."""
  paymentMode_in: [String!]

  """All values that are not contained in given list."""
  paymentMode_not_in: [String!]

  """All values less than the given value."""
  paymentMode_lt: String

  """All values less than or equal the given value."""
  paymentMode_lte: String

  """All values greater than the given value."""
  paymentMode_gt: String

  """All values greater than or equal the given value."""
  paymentMode_gte: String

  """All values containing the given string."""
  paymentMode_contains: String

  """All values not containing the given string."""
  paymentMode_not_contains: String

  """All values starting with the given string."""
  paymentMode_starts_with: String

  """All values not starting with the given string."""
  paymentMode_not_starts_with: String

  """All values ending with the given string."""
  paymentMode_ends_with: String

  """All values not ending with the given string."""
  paymentMode_not_ends_with: String
  supplier: String

  """All values that are not equal to given value."""
  supplier_not: String

  """All values that are contained in given list."""
  supplier_in: [String!]

  """All values that are not contained in given list."""
  supplier_not_in: [String!]

  """All values less than the given value."""
  supplier_lt: String

  """All values less than or equal the given value."""
  supplier_lte: String

  """All values greater than the given value."""
  supplier_gt: String

  """All values greater than or equal the given value."""
  supplier_gte: String

  """All values containing the given string."""
  supplier_contains: String

  """All values not containing the given string."""
  supplier_not_contains: String

  """All values starting with the given string."""
  supplier_starts_with: String

  """All values not starting with the given string."""
  supplier_not_starts_with: String

  """All values ending with the given string."""
  supplier_ends_with: String

  """All values not ending with the given string."""
  supplier_not_ends_with: String
  amount: Float

  """All values that are not equal to given value."""
  amount_not: Float

  """All values that are contained in given list."""
  amount_in: [Float!]

  """All values that are not contained in given list."""
  amount_not_in: [Float!]

  """All values less than the given value."""
  amount_lt: Float

  """All values less than or equal the given value."""
  amount_lte: Float

  """All values greater than the given value."""
  amount_gt: Float

  """All values greater than or equal the given value."""
  amount_gte: Float
  description: String

  """All values that are not equal to given value."""
  description_not: String

  """All values that are contained in given list."""
  description_in: [String!]

  """All values that are not contained in given list."""
  description_not_in: [String!]

  """All values less than the given value."""
  description_lt: String

  """All values less than or equal the given value."""
  description_lte: String

  """All values greater than the given value."""
  description_gt: String

  """All values greater than or equal the given value."""
  description_gte: String

  """All values containing the given string."""
  description_contains: String

  """All values not containing the given string."""
  description_not_contains: String

  """All values starting with the given string."""
  description_starts_with: String

  """All values not starting with the given string."""
  description_not_starts_with: String

  """All values ending with the given string."""
  description_ends_with: String

  """All values not ending with the given string."""
  description_not_ends_with: String
  observation: String

  """All values that are not equal to given value."""
  observation_not: String

  """All values that are contained in given list."""
  observation_in: [String!]

  """All values that are not contained in given list."""
  observation_not_in: [String!]

  """All values less than the given value."""
  observation_lt: String

  """All values less than or equal the given value."""
  observation_lte: String

  """All values greater than the given value."""
  observation_gt: String

  """All values greater than or equal the given value."""
  observation_gte: String

  """All values containing the given string."""
  observation_contains: String

  """All values not containing the given string."""
  observation_not_contains: String

  """All values starting with the given string."""
  observation_starts_with: String

  """All values not starting with the given string."""
  observation_not_starts_with: String

  """All values ending with the given string."""
  observation_ends_with: String

  """All values not ending with the given string."""
  observation_not_ends_with: String
  category: SpendingType

  """All values that are not equal to given value."""
  category_not: SpendingType

  """All values that are contained in given list."""
  category_in: [SpendingType!]

  """All values that are not contained in given list."""
  category_not_in: [SpendingType!]
}

type SpendingEntrySubscriptionPayload {
  mutation: MutationType!
  node: SpendingEntry
  updatedFields: [String!]
  previousValues: SpendingEntryPreviousValues
}

input SpendingEntrySubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [SpendingEntrySubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [SpendingEntrySubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [SpendingEntrySubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: SpendingEntryWhereInput
}

input SpendingEntryUpdateInput {
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  category: SpendingType
  document: DocumentUpdateOneWithoutSpendingEntriesInput
}

input SpendingEntryUpdateManyDataInput {
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  category: SpendingType
}

input SpendingEntryUpdateManyMutationInput {
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  category: SpendingType
}

input SpendingEntryUpdateManyWithoutDocumentInput {
  create: [SpendingEntryCreateWithoutDocumentInput!]
  connect: [SpendingEntryWhereUniqueInput!]
  set: [SpendingEntryWhereUniqueInput!]
  disconnect: [SpendingEntryWhereUniqueInput!]
  delete: [SpendingEntryWhereUniqueInput!]
  update: [SpendingEntryUpdateWithWhereUniqueWithoutDocumentInput!]
  updateMany: [SpendingEntryUpdateManyWithWhereNestedInput!]
  deleteMany: [SpendingEntryScalarWhereInput!]
  upsert: [SpendingEntryUpsertWithWhereUniqueWithoutDocumentInput!]
}

input SpendingEntryUpdateManyWithWhereNestedInput {
  where: SpendingEntryScalarWhereInput!
  data: SpendingEntryUpdateManyDataInput!
}

input SpendingEntryUpdateWithoutDocumentDataInput {
  reference: String
  designation: String
  date: DateTime
  paymentMode: String
  supplier: String
  amount: Float
  description: String
  observation: String
  category: SpendingType
}

input SpendingEntryUpdateWithWhereUniqueWithoutDocumentInput {
  where: SpendingEntryWhereUniqueInput!
  data: SpendingEntryUpdateWithoutDocumentDataInput!
}

input SpendingEntryUpsertWithWhereUniqueWithoutDocumentInput {
  where: SpendingEntryWhereUniqueInput!
  update: SpendingEntryUpdateWithoutDocumentDataInput!
  create: SpendingEntryCreateWithoutDocumentInput!
}

input SpendingEntryWhereInput {
  """Logical AND on all given filters."""
  AND: [SpendingEntryWhereInput!]

  """Logical OR on all given filters."""
  OR: [SpendingEntryWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [SpendingEntryWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  reference: String

  """All values that are not equal to given value."""
  reference_not: String

  """All values that are contained in given list."""
  reference_in: [String!]

  """All values that are not contained in given list."""
  reference_not_in: [String!]

  """All values less than the given value."""
  reference_lt: String

  """All values less than or equal the given value."""
  reference_lte: String

  """All values greater than the given value."""
  reference_gt: String

  """All values greater than or equal the given value."""
  reference_gte: String

  """All values containing the given string."""
  reference_contains: String

  """All values not containing the given string."""
  reference_not_contains: String

  """All values starting with the given string."""
  reference_starts_with: String

  """All values not starting with the given string."""
  reference_not_starts_with: String

  """All values ending with the given string."""
  reference_ends_with: String

  """All values not ending with the given string."""
  reference_not_ends_with: String
  designation: String

  """All values that are not equal to given value."""
  designation_not: String

  """All values that are contained in given list."""
  designation_in: [String!]

  """All values that are not contained in given list."""
  designation_not_in: [String!]

  """All values less than the given value."""
  designation_lt: String

  """All values less than or equal the given value."""
  designation_lte: String

  """All values greater than the given value."""
  designation_gt: String

  """All values greater than or equal the given value."""
  designation_gte: String

  """All values containing the given string."""
  designation_contains: String

  """All values not containing the given string."""
  designation_not_contains: String

  """All values starting with the given string."""
  designation_starts_with: String

  """All values not starting with the given string."""
  designation_not_starts_with: String

  """All values ending with the given string."""
  designation_ends_with: String

  """All values not ending with the given string."""
  designation_not_ends_with: String
  date: DateTime

  """All values that are not equal to given value."""
  date_not: DateTime

  """All values that are contained in given list."""
  date_in: [DateTime!]

  """All values that are not contained in given list."""
  date_not_in: [DateTime!]

  """All values less than the given value."""
  date_lt: DateTime

  """All values less than or equal the given value."""
  date_lte: DateTime

  """All values greater than the given value."""
  date_gt: DateTime

  """All values greater than or equal the given value."""
  date_gte: DateTime
  paymentMode: String

  """All values that are not equal to given value."""
  paymentMode_not: String

  """All values that are contained in given list."""
  paymentMode_in: [String!]

  """All values that are not contained in given list."""
  paymentMode_not_in: [String!]

  """All values less than the given value."""
  paymentMode_lt: String

  """All values less than or equal the given value."""
  paymentMode_lte: String

  """All values greater than the given value."""
  paymentMode_gt: String

  """All values greater than or equal the given value."""
  paymentMode_gte: String

  """All values containing the given string."""
  paymentMode_contains: String

  """All values not containing the given string."""
  paymentMode_not_contains: String

  """All values starting with the given string."""
  paymentMode_starts_with: String

  """All values not starting with the given string."""
  paymentMode_not_starts_with: String

  """All values ending with the given string."""
  paymentMode_ends_with: String

  """All values not ending with the given string."""
  paymentMode_not_ends_with: String
  supplier: String

  """All values that are not equal to given value."""
  supplier_not: String

  """All values that are contained in given list."""
  supplier_in: [String!]

  """All values that are not contained in given list."""
  supplier_not_in: [String!]

  """All values less than the given value."""
  supplier_lt: String

  """All values less than or equal the given value."""
  supplier_lte: String

  """All values greater than the given value."""
  supplier_gt: String

  """All values greater than or equal the given value."""
  supplier_gte: String

  """All values containing the given string."""
  supplier_contains: String

  """All values not containing the given string."""
  supplier_not_contains: String

  """All values starting with the given string."""
  supplier_starts_with: String

  """All values not starting with the given string."""
  supplier_not_starts_with: String

  """All values ending with the given string."""
  supplier_ends_with: String

  """All values not ending with the given string."""
  supplier_not_ends_with: String
  amount: Float

  """All values that are not equal to given value."""
  amount_not: Float

  """All values that are contained in given list."""
  amount_in: [Float!]

  """All values that are not contained in given list."""
  amount_not_in: [Float!]

  """All values less than the given value."""
  amount_lt: Float

  """All values less than or equal the given value."""
  amount_lte: Float

  """All values greater than the given value."""
  amount_gt: Float

  """All values greater than or equal the given value."""
  amount_gte: Float
  description: String

  """All values that are not equal to given value."""
  description_not: String

  """All values that are contained in given list."""
  description_in: [String!]

  """All values that are not contained in given list."""
  description_not_in: [String!]

  """All values less than the given value."""
  description_lt: String

  """All values less than or equal the given value."""
  description_lte: String

  """All values greater than the given value."""
  description_gt: String

  """All values greater than or equal the given value."""
  description_gte: String

  """All values containing the given string."""
  description_contains: String

  """All values not containing the given string."""
  description_not_contains: String

  """All values starting with the given string."""
  description_starts_with: String

  """All values not starting with the given string."""
  description_not_starts_with: String

  """All values ending with the given string."""
  description_ends_with: String

  """All values not ending with the given string."""
  description_not_ends_with: String
  observation: String

  """All values that are not equal to given value."""
  observation_not: String

  """All values that are contained in given list."""
  observation_in: [String!]

  """All values that are not contained in given list."""
  observation_not_in: [String!]

  """All values less than the given value."""
  observation_lt: String

  """All values less than or equal the given value."""
  observation_lte: String

  """All values greater than the given value."""
  observation_gt: String

  """All values greater than or equal the given value."""
  observation_gte: String

  """All values containing the given string."""
  observation_contains: String

  """All values not containing the given string."""
  observation_not_contains: String

  """All values starting with the given string."""
  observation_starts_with: String

  """All values not starting with the given string."""
  observation_not_starts_with: String

  """All values ending with the given string."""
  observation_ends_with: String

  """All values not ending with the given string."""
  observation_not_ends_with: String
  category: SpendingType

  """All values that are not equal to given value."""
  category_not: SpendingType

  """All values that are contained in given list."""
  category_in: [SpendingType!]

  """All values that are not contained in given list."""
  category_not_in: [SpendingType!]
  document: DocumentWhereInput
}

input SpendingEntryWhereUniqueInput {
  id: ID
}

enum SpendingType {
  PERM
  MOVE
  MEAL
  LEARN
  COM
  REC
  PPL
  END
  MISC
  UNJUST
}

type Subscription {
  user(where: UserSubscriptionWhereInput): UserSubscriptionPayload
  agenda(where: AgendaSubscriptionWhereInput): AgendaSubscriptionPayload
  document(where: DocumentSubscriptionWhereInput): DocumentSubscriptionPayload
  spendingEntry(where: SpendingEntrySubscriptionWhereInput): SpendingEntrySubscriptionPayload
  metric(where: MetricSubscriptionWhereInput): MetricSubscriptionPayload
}

type User implements Node {
  id: ID!
  mail: String!
  name: String!
  password: String!
  agendas(where: AgendaWhereInput, orderBy: AgendaOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Agenda!]
  documents(where: DocumentWhereInput, orderBy: DocumentOrderByInput, skip: Int, after: String, before: String, first: Int, last: Int): [Document!]
  updatedAt: DateTime!
  createdAt: DateTime!
}

"""A connection to a list of items."""
type UserConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [UserEdge]!
  aggregate: AggregateUser!
}

input UserCreateInput {
  id: ID
  mail: String!
  name: String!
  password: String!
  agendas: AgendaCreateManyWithoutUserInput
  documents: DocumentCreateManyWithoutUserInput
}

input UserCreateOneWithoutAgendasInput {
  create: UserCreateWithoutAgendasInput
  connect: UserWhereUniqueInput
}

input UserCreateOneWithoutDocumentsInput {
  create: UserCreateWithoutDocumentsInput
  connect: UserWhereUniqueInput
}

input UserCreateWithoutAgendasInput {
  id: ID
  mail: String!
  name: String!
  password: String!
  documents: DocumentCreateManyWithoutUserInput
}

input UserCreateWithoutDocumentsInput {
  id: ID
  mail: String!
  name: String!
  password: String!
  agendas: AgendaCreateManyWithoutUserInput
}

"""An edge in a connection."""
type UserEdge {
  """The item at the end of the edge."""
  node: User!

  """A cursor for use in pagination."""
  cursor: String!
}

enum UserOrderByInput {
  id_ASC
  id_DESC
  mail_ASC
  mail_DESC
  name_ASC
  name_DESC
  password_ASC
  password_DESC
  updatedAt_ASC
  updatedAt_DESC
  createdAt_ASC
  createdAt_DESC
}

type UserPreviousValues {
  id: ID!
  mail: String!
  name: String!
  password: String!
  updatedAt: DateTime!
  createdAt: DateTime!
}

type UserSubscriptionPayload {
  mutation: MutationType!
  node: User
  updatedFields: [String!]
  previousValues: UserPreviousValues
}

input UserSubscriptionWhereInput {
  """Logical AND on all given filters."""
  AND: [UserSubscriptionWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserSubscriptionWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserSubscriptionWhereInput!]

  """
  The subscription event gets dispatched when it's listed in mutation_in
  """
  mutation_in: [MutationType!]

  """
  The subscription event gets only dispatched when one of the updated fields names is included in this list
  """
  updatedFields_contains: String

  """
  The subscription event gets only dispatched when all of the field names included in this list have been updated
  """
  updatedFields_contains_every: [String!]

  """
  The subscription event gets only dispatched when some of the field names included in this list have been updated
  """
  updatedFields_contains_some: [String!]
  node: UserWhereInput
}

input UserUpdateInput {
  mail: String
  name: String
  password: String
  agendas: AgendaUpdateManyWithoutUserInput
  documents: DocumentUpdateManyWithoutUserInput
}

input UserUpdateManyMutationInput {
  mail: String
  name: String
  password: String
}

input UserUpdateOneRequiredWithoutAgendasInput {
  create: UserCreateWithoutAgendasInput
  connect: UserWhereUniqueInput
  update: UserUpdateWithoutAgendasDataInput
  upsert: UserUpsertWithoutAgendasInput
}

input UserUpdateOneRequiredWithoutDocumentsInput {
  create: UserCreateWithoutDocumentsInput
  connect: UserWhereUniqueInput
  update: UserUpdateWithoutDocumentsDataInput
  upsert: UserUpsertWithoutDocumentsInput
}

input UserUpdateWithoutAgendasDataInput {
  mail: String
  name: String
  password: String
  documents: DocumentUpdateManyWithoutUserInput
}

input UserUpdateWithoutDocumentsDataInput {
  mail: String
  name: String
  password: String
  agendas: AgendaUpdateManyWithoutUserInput
}

input UserUpsertWithoutAgendasInput {
  update: UserUpdateWithoutAgendasDataInput!
  create: UserCreateWithoutAgendasInput!
}

input UserUpsertWithoutDocumentsInput {
  update: UserUpdateWithoutDocumentsDataInput!
  create: UserCreateWithoutDocumentsInput!
}

input UserWhereInput {
  """Logical AND on all given filters."""
  AND: [UserWhereInput!]

  """Logical OR on all given filters."""
  OR: [UserWhereInput!]

  """Logical NOT on all given filters combined by AND."""
  NOT: [UserWhereInput!]
  id: ID

  """All values that are not equal to given value."""
  id_not: ID

  """All values that are contained in given list."""
  id_in: [ID!]

  """All values that are not contained in given list."""
  id_not_in: [ID!]

  """All values less than the given value."""
  id_lt: ID

  """All values less than or equal the given value."""
  id_lte: ID

  """All values greater than the given value."""
  id_gt: ID

  """All values greater than or equal the given value."""
  id_gte: ID

  """All values containing the given string."""
  id_contains: ID

  """All values not containing the given string."""
  id_not_contains: ID

  """All values starting with the given string."""
  id_starts_with: ID

  """All values not starting with the given string."""
  id_not_starts_with: ID

  """All values ending with the given string."""
  id_ends_with: ID

  """All values not ending with the given string."""
  id_not_ends_with: ID
  mail: String

  """All values that are not equal to given value."""
  mail_not: String

  """All values that are contained in given list."""
  mail_in: [String!]

  """All values that are not contained in given list."""
  mail_not_in: [String!]

  """All values less than the given value."""
  mail_lt: String

  """All values less than or equal the given value."""
  mail_lte: String

  """All values greater than the given value."""
  mail_gt: String

  """All values greater than or equal the given value."""
  mail_gte: String

  """All values containing the given string."""
  mail_contains: String

  """All values not containing the given string."""
  mail_not_contains: String

  """All values starting with the given string."""
  mail_starts_with: String

  """All values not starting with the given string."""
  mail_not_starts_with: String

  """All values ending with the given string."""
  mail_ends_with: String

  """All values not ending with the given string."""
  mail_not_ends_with: String
  name: String

  """All values that are not equal to given value."""
  name_not: String

  """All values that are contained in given list."""
  name_in: [String!]

  """All values that are not contained in given list."""
  name_not_in: [String!]

  """All values less than the given value."""
  name_lt: String

  """All values less than or equal the given value."""
  name_lte: String

  """All values greater than the given value."""
  name_gt: String

  """All values greater than or equal the given value."""
  name_gte: String

  """All values containing the given string."""
  name_contains: String

  """All values not containing the given string."""
  name_not_contains: String

  """All values starting with the given string."""
  name_starts_with: String

  """All values not starting with the given string."""
  name_not_starts_with: String

  """All values ending with the given string."""
  name_ends_with: String

  """All values not ending with the given string."""
  name_not_ends_with: String
  password: String

  """All values that are not equal to given value."""
  password_not: String

  """All values that are contained in given list."""
  password_in: [String!]

  """All values that are not contained in given list."""
  password_not_in: [String!]

  """All values less than the given value."""
  password_lt: String

  """All values less than or equal the given value."""
  password_lte: String

  """All values greater than the given value."""
  password_gt: String

  """All values greater than or equal the given value."""
  password_gte: String

  """All values containing the given string."""
  password_contains: String

  """All values not containing the given string."""
  password_not_contains: String

  """All values starting with the given string."""
  password_starts_with: String

  """All values not starting with the given string."""
  password_not_starts_with: String

  """All values ending with the given string."""
  password_ends_with: String

  """All values not ending with the given string."""
  password_not_ends_with: String
  updatedAt: DateTime

  """All values that are not equal to given value."""
  updatedAt_not: DateTime

  """All values that are contained in given list."""
  updatedAt_in: [DateTime!]

  """All values that are not contained in given list."""
  updatedAt_not_in: [DateTime!]

  """All values less than the given value."""
  updatedAt_lt: DateTime

  """All values less than or equal the given value."""
  updatedAt_lte: DateTime

  """All values greater than the given value."""
  updatedAt_gt: DateTime

  """All values greater than or equal the given value."""
  updatedAt_gte: DateTime
  createdAt: DateTime

  """All values that are not equal to given value."""
  createdAt_not: DateTime

  """All values that are contained in given list."""
  createdAt_in: [DateTime!]

  """All values that are not contained in given list."""
  createdAt_not_in: [DateTime!]

  """All values less than the given value."""
  createdAt_lt: DateTime

  """All values less than or equal the given value."""
  createdAt_lte: DateTime

  """All values greater than the given value."""
  createdAt_gt: DateTime

  """All values greater than or equal the given value."""
  createdAt_gte: DateTime
  agendas_every: AgendaWhereInput
  agendas_some: AgendaWhereInput
  agendas_none: AgendaWhereInput
  documents_every: DocumentWhereInput
  documents_some: DocumentWhereInput
  documents_none: DocumentWhereInput
}

input UserWhereUniqueInput {
  id: ID
  mail: String
  name: String
}
`;

export const Prisma = makePrismaBindingClass<BindingConstructor<Prisma>>({
  typeDefs
});

/**
 * Types
 */

export type AgendaOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "name_ASC"
  | "name_DESC"
  | "url_ASC"
  | "url_DESC"
  | "updatedAt_ASC"
  | "updatedAt_DESC"
  | "createdAt_ASC"
  | "createdAt_DESC";

export type DocumentOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "name_ASC"
  | "name_DESC"
  | "filename_ASC"
  | "filename_DESC"
  | "type_ASC"
  | "type_DESC"
  | "updatedAt_ASC"
  | "updatedAt_DESC"
  | "createdAt_ASC"
  | "createdAt_DESC";

export type DocumentType = "AFM";

export type MetricOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "value_ASC"
  | "value_DESC"
  | "type_ASC"
  | "type_DESC";

export type MutationType = "CREATED" | "UPDATED" | "DELETED";

export type SpendingEntryOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "reference_ASC"
  | "reference_DESC"
  | "designation_ASC"
  | "designation_DESC"
  | "date_ASC"
  | "date_DESC"
  | "paymentMode_ASC"
  | "paymentMode_DESC"
  | "supplier_ASC"
  | "supplier_DESC"
  | "amount_ASC"
  | "amount_DESC"
  | "description_ASC"
  | "description_DESC"
  | "observation_ASC"
  | "observation_DESC"
  | "category_ASC"
  | "category_DESC";

export type SpendingType =
  | "PERM"
  | "MOVE"
  | "MEAL"
  | "LEARN"
  | "COM"
  | "REC"
  | "PPL"
  | "END"
  | "MISC"
  | "UNJUST";

export type UserOrderByInput =
  | "id_ASC"
  | "id_DESC"
  | "mail_ASC"
  | "mail_DESC"
  | "name_ASC"
  | "name_DESC"
  | "password_ASC"
  | "password_DESC"
  | "updatedAt_ASC"
  | "updatedAt_DESC"
  | "createdAt_ASC"
  | "createdAt_DESC";

export interface AgendaCreateInput {
  id?: ID_Input | null;
  name?: String | null;
  url?: String | null;
  user: UserCreateOneWithoutAgendasInput;
}

export interface AgendaCreateManyWithoutUserInput {
  create?: AgendaCreateWithoutUserInput[] | AgendaCreateWithoutUserInput | null;
  connect?: AgendaWhereUniqueInput[] | AgendaWhereUniqueInput | null;
}

export interface AgendaCreateWithoutUserInput {
  id?: ID_Input | null;
  name?: String | null;
  url?: String | null;
}

export interface AgendaScalarWhereInput {
  AND?: AgendaScalarWhereInput[] | AgendaScalarWhereInput | null;
  OR?: AgendaScalarWhereInput[] | AgendaScalarWhereInput | null;
  NOT?: AgendaScalarWhereInput[] | AgendaScalarWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  name?: String | null;
  name_not?: String | null;
  name_in?: String[] | String | null;
  name_not_in?: String[] | String | null;
  name_lt?: String | null;
  name_lte?: String | null;
  name_gt?: String | null;
  name_gte?: String | null;
  name_contains?: String | null;
  name_not_contains?: String | null;
  name_starts_with?: String | null;
  name_not_starts_with?: String | null;
  name_ends_with?: String | null;
  name_not_ends_with?: String | null;
  url?: String | null;
  url_not?: String | null;
  url_in?: String[] | String | null;
  url_not_in?: String[] | String | null;
  url_lt?: String | null;
  url_lte?: String | null;
  url_gt?: String | null;
  url_gte?: String | null;
  url_contains?: String | null;
  url_not_contains?: String | null;
  url_starts_with?: String | null;
  url_not_starts_with?: String | null;
  url_ends_with?: String | null;
  url_not_ends_with?: String | null;
  updatedAt?: DateTime | null;
  updatedAt_not?: DateTime | null;
  updatedAt_in?: DateTime[] | DateTime | null;
  updatedAt_not_in?: DateTime[] | DateTime | null;
  updatedAt_lt?: DateTime | null;
  updatedAt_lte?: DateTime | null;
  updatedAt_gt?: DateTime | null;
  updatedAt_gte?: DateTime | null;
  createdAt?: DateTime | null;
  createdAt_not?: DateTime | null;
  createdAt_in?: DateTime[] | DateTime | null;
  createdAt_not_in?: DateTime[] | DateTime | null;
  createdAt_lt?: DateTime | null;
  createdAt_lte?: DateTime | null;
  createdAt_gt?: DateTime | null;
  createdAt_gte?: DateTime | null;
}

export interface AgendaSubscriptionWhereInput {
  AND?: AgendaSubscriptionWhereInput[] | AgendaSubscriptionWhereInput | null;
  OR?: AgendaSubscriptionWhereInput[] | AgendaSubscriptionWhereInput | null;
  NOT?: AgendaSubscriptionWhereInput[] | AgendaSubscriptionWhereInput | null;
  mutation_in?: MutationType[] | MutationType | null;
  updatedFields_contains?: String | null;
  updatedFields_contains_every?: String[] | String | null;
  updatedFields_contains_some?: String[] | String | null;
  node?: AgendaWhereInput | null;
}

export interface AgendaUpdateInput {
  name?: String | null;
  url?: String | null;
  user?: UserUpdateOneRequiredWithoutAgendasInput | null;
}

export interface AgendaUpdateManyDataInput {
  name?: String | null;
  url?: String | null;
}

export interface AgendaUpdateManyMutationInput {
  name?: String | null;
  url?: String | null;
}

export interface AgendaUpdateManyWithoutUserInput {
  create?: AgendaCreateWithoutUserInput[] | AgendaCreateWithoutUserInput | null;
  connect?: AgendaWhereUniqueInput[] | AgendaWhereUniqueInput | null;
  set?: AgendaWhereUniqueInput[] | AgendaWhereUniqueInput | null;
  disconnect?: AgendaWhereUniqueInput[] | AgendaWhereUniqueInput | null;
  delete?: AgendaWhereUniqueInput[] | AgendaWhereUniqueInput | null;
  update?:
    | AgendaUpdateWithWhereUniqueWithoutUserInput[]
    | AgendaUpdateWithWhereUniqueWithoutUserInput
    | null;
  updateMany?:
    | AgendaUpdateManyWithWhereNestedInput[]
    | AgendaUpdateManyWithWhereNestedInput
    | null;
  deleteMany?: AgendaScalarWhereInput[] | AgendaScalarWhereInput | null;
  upsert?:
    | AgendaUpsertWithWhereUniqueWithoutUserInput[]
    | AgendaUpsertWithWhereUniqueWithoutUserInput
    | null;
}

export interface AgendaUpdateManyWithWhereNestedInput {
  where: AgendaScalarWhereInput;
  data: AgendaUpdateManyDataInput;
}

export interface AgendaUpdateWithoutUserDataInput {
  name?: String | null;
  url?: String | null;
}

export interface AgendaUpdateWithWhereUniqueWithoutUserInput {
  where: AgendaWhereUniqueInput;
  data: AgendaUpdateWithoutUserDataInput;
}

export interface AgendaUpsertWithWhereUniqueWithoutUserInput {
  where: AgendaWhereUniqueInput;
  update: AgendaUpdateWithoutUserDataInput;
  create: AgendaCreateWithoutUserInput;
}

export interface AgendaWhereInput {
  AND?: AgendaWhereInput[] | AgendaWhereInput | null;
  OR?: AgendaWhereInput[] | AgendaWhereInput | null;
  NOT?: AgendaWhereInput[] | AgendaWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  name?: String | null;
  name_not?: String | null;
  name_in?: String[] | String | null;
  name_not_in?: String[] | String | null;
  name_lt?: String | null;
  name_lte?: String | null;
  name_gt?: String | null;
  name_gte?: String | null;
  name_contains?: String | null;
  name_not_contains?: String | null;
  name_starts_with?: String | null;
  name_not_starts_with?: String | null;
  name_ends_with?: String | null;
  name_not_ends_with?: String | null;
  url?: String | null;
  url_not?: String | null;
  url_in?: String[] | String | null;
  url_not_in?: String[] | String | null;
  url_lt?: String | null;
  url_lte?: String | null;
  url_gt?: String | null;
  url_gte?: String | null;
  url_contains?: String | null;
  url_not_contains?: String | null;
  url_starts_with?: String | null;
  url_not_starts_with?: String | null;
  url_ends_with?: String | null;
  url_not_ends_with?: String | null;
  updatedAt?: DateTime | null;
  updatedAt_not?: DateTime | null;
  updatedAt_in?: DateTime[] | DateTime | null;
  updatedAt_not_in?: DateTime[] | DateTime | null;
  updatedAt_lt?: DateTime | null;
  updatedAt_lte?: DateTime | null;
  updatedAt_gt?: DateTime | null;
  updatedAt_gte?: DateTime | null;
  createdAt?: DateTime | null;
  createdAt_not?: DateTime | null;
  createdAt_in?: DateTime[] | DateTime | null;
  createdAt_not_in?: DateTime[] | DateTime | null;
  createdAt_lt?: DateTime | null;
  createdAt_lte?: DateTime | null;
  createdAt_gt?: DateTime | null;
  createdAt_gte?: DateTime | null;
  user?: UserWhereInput | null;
}

export interface AgendaWhereUniqueInput {
  id?: ID_Input | null;
}

export interface DocumentCreateInput {
  id?: ID_Input | null;
  name?: String | null;
  filename: String;
  type?: DocumentType | null;
  user: UserCreateOneWithoutDocumentsInput;
  metrics?: MetricCreateManyWithoutDocumentInput | null;
  spendingEntries?: SpendingEntryCreateManyWithoutDocumentInput | null;
}

export interface DocumentCreateManyWithoutUserInput {
  create?:
    | DocumentCreateWithoutUserInput[]
    | DocumentCreateWithoutUserInput
    | null;
  connect?: DocumentWhereUniqueInput[] | DocumentWhereUniqueInput | null;
}

export interface DocumentCreateOneWithoutMetricsInput {
  create?: DocumentCreateWithoutMetricsInput | null;
  connect?: DocumentWhereUniqueInput | null;
}

export interface DocumentCreateOneWithoutSpendingEntriesInput {
  create?: DocumentCreateWithoutSpendingEntriesInput | null;
  connect?: DocumentWhereUniqueInput | null;
}

export interface DocumentCreateWithoutMetricsInput {
  id?: ID_Input | null;
  name?: String | null;
  filename: String;
  type?: DocumentType | null;
  user: UserCreateOneWithoutDocumentsInput;
  spendingEntries?: SpendingEntryCreateManyWithoutDocumentInput | null;
}

export interface DocumentCreateWithoutSpendingEntriesInput {
  id?: ID_Input | null;
  name?: String | null;
  filename: String;
  type?: DocumentType | null;
  user: UserCreateOneWithoutDocumentsInput;
  metrics?: MetricCreateManyWithoutDocumentInput | null;
}

export interface DocumentCreateWithoutUserInput {
  id?: ID_Input | null;
  name?: String | null;
  filename: String;
  type?: DocumentType | null;
  metrics?: MetricCreateManyWithoutDocumentInput | null;
  spendingEntries?: SpendingEntryCreateManyWithoutDocumentInput | null;
}

export interface DocumentScalarWhereInput {
  AND?: DocumentScalarWhereInput[] | DocumentScalarWhereInput | null;
  OR?: DocumentScalarWhereInput[] | DocumentScalarWhereInput | null;
  NOT?: DocumentScalarWhereInput[] | DocumentScalarWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  name?: String | null;
  name_not?: String | null;
  name_in?: String[] | String | null;
  name_not_in?: String[] | String | null;
  name_lt?: String | null;
  name_lte?: String | null;
  name_gt?: String | null;
  name_gte?: String | null;
  name_contains?: String | null;
  name_not_contains?: String | null;
  name_starts_with?: String | null;
  name_not_starts_with?: String | null;
  name_ends_with?: String | null;
  name_not_ends_with?: String | null;
  filename?: String | null;
  filename_not?: String | null;
  filename_in?: String[] | String | null;
  filename_not_in?: String[] | String | null;
  filename_lt?: String | null;
  filename_lte?: String | null;
  filename_gt?: String | null;
  filename_gte?: String | null;
  filename_contains?: String | null;
  filename_not_contains?: String | null;
  filename_starts_with?: String | null;
  filename_not_starts_with?: String | null;
  filename_ends_with?: String | null;
  filename_not_ends_with?: String | null;
  type?: DocumentType | null;
  type_not?: DocumentType | null;
  type_in?: DocumentType[] | DocumentType | null;
  type_not_in?: DocumentType[] | DocumentType | null;
  updatedAt?: DateTime | null;
  updatedAt_not?: DateTime | null;
  updatedAt_in?: DateTime[] | DateTime | null;
  updatedAt_not_in?: DateTime[] | DateTime | null;
  updatedAt_lt?: DateTime | null;
  updatedAt_lte?: DateTime | null;
  updatedAt_gt?: DateTime | null;
  updatedAt_gte?: DateTime | null;
  createdAt?: DateTime | null;
  createdAt_not?: DateTime | null;
  createdAt_in?: DateTime[] | DateTime | null;
  createdAt_not_in?: DateTime[] | DateTime | null;
  createdAt_lt?: DateTime | null;
  createdAt_lte?: DateTime | null;
  createdAt_gt?: DateTime | null;
  createdAt_gte?: DateTime | null;
}

export interface DocumentSubscriptionWhereInput {
  AND?:
    | DocumentSubscriptionWhereInput[]
    | DocumentSubscriptionWhereInput
    | null;
  OR?: DocumentSubscriptionWhereInput[] | DocumentSubscriptionWhereInput | null;
  NOT?:
    | DocumentSubscriptionWhereInput[]
    | DocumentSubscriptionWhereInput
    | null;
  mutation_in?: MutationType[] | MutationType | null;
  updatedFields_contains?: String | null;
  updatedFields_contains_every?: String[] | String | null;
  updatedFields_contains_some?: String[] | String | null;
  node?: DocumentWhereInput | null;
}

export interface DocumentUpdateInput {
  name?: String | null;
  filename?: String | null;
  type?: DocumentType | null;
  user?: UserUpdateOneRequiredWithoutDocumentsInput | null;
  metrics?: MetricUpdateManyWithoutDocumentInput | null;
  spendingEntries?: SpendingEntryUpdateManyWithoutDocumentInput | null;
}

export interface DocumentUpdateManyDataInput {
  name?: String | null;
  filename?: String | null;
  type?: DocumentType | null;
}

export interface DocumentUpdateManyMutationInput {
  name?: String | null;
  filename?: String | null;
  type?: DocumentType | null;
}

export interface DocumentUpdateManyWithoutUserInput {
  create?:
    | DocumentCreateWithoutUserInput[]
    | DocumentCreateWithoutUserInput
    | null;
  connect?: DocumentWhereUniqueInput[] | DocumentWhereUniqueInput | null;
  set?: DocumentWhereUniqueInput[] | DocumentWhereUniqueInput | null;
  disconnect?: DocumentWhereUniqueInput[] | DocumentWhereUniqueInput | null;
  delete?: DocumentWhereUniqueInput[] | DocumentWhereUniqueInput | null;
  update?:
    | DocumentUpdateWithWhereUniqueWithoutUserInput[]
    | DocumentUpdateWithWhereUniqueWithoutUserInput
    | null;
  updateMany?:
    | DocumentUpdateManyWithWhereNestedInput[]
    | DocumentUpdateManyWithWhereNestedInput
    | null;
  deleteMany?: DocumentScalarWhereInput[] | DocumentScalarWhereInput | null;
  upsert?:
    | DocumentUpsertWithWhereUniqueWithoutUserInput[]
    | DocumentUpsertWithWhereUniqueWithoutUserInput
    | null;
}

export interface DocumentUpdateManyWithWhereNestedInput {
  where: DocumentScalarWhereInput;
  data: DocumentUpdateManyDataInput;
}

export interface DocumentUpdateOneRequiredWithoutMetricsInput {
  create?: DocumentCreateWithoutMetricsInput | null;
  connect?: DocumentWhereUniqueInput | null;
  update?: DocumentUpdateWithoutMetricsDataInput | null;
  upsert?: DocumentUpsertWithoutMetricsInput | null;
}

export interface DocumentUpdateOneWithoutSpendingEntriesInput {
  create?: DocumentCreateWithoutSpendingEntriesInput | null;
  connect?: DocumentWhereUniqueInput | null;
  disconnect?: Boolean | null;
  delete?: Boolean | null;
  update?: DocumentUpdateWithoutSpendingEntriesDataInput | null;
  upsert?: DocumentUpsertWithoutSpendingEntriesInput | null;
}

export interface DocumentUpdateWithoutMetricsDataInput {
  name?: String | null;
  filename?: String | null;
  type?: DocumentType | null;
  user?: UserUpdateOneRequiredWithoutDocumentsInput | null;
  spendingEntries?: SpendingEntryUpdateManyWithoutDocumentInput | null;
}

export interface DocumentUpdateWithoutSpendingEntriesDataInput {
  name?: String | null;
  filename?: String | null;
  type?: DocumentType | null;
  user?: UserUpdateOneRequiredWithoutDocumentsInput | null;
  metrics?: MetricUpdateManyWithoutDocumentInput | null;
}

export interface DocumentUpdateWithoutUserDataInput {
  name?: String | null;
  filename?: String | null;
  type?: DocumentType | null;
  metrics?: MetricUpdateManyWithoutDocumentInput | null;
  spendingEntries?: SpendingEntryUpdateManyWithoutDocumentInput | null;
}

export interface DocumentUpdateWithWhereUniqueWithoutUserInput {
  where: DocumentWhereUniqueInput;
  data: DocumentUpdateWithoutUserDataInput;
}

export interface DocumentUpsertWithoutMetricsInput {
  update: DocumentUpdateWithoutMetricsDataInput;
  create: DocumentCreateWithoutMetricsInput;
}

export interface DocumentUpsertWithoutSpendingEntriesInput {
  update: DocumentUpdateWithoutSpendingEntriesDataInput;
  create: DocumentCreateWithoutSpendingEntriesInput;
}

export interface DocumentUpsertWithWhereUniqueWithoutUserInput {
  where: DocumentWhereUniqueInput;
  update: DocumentUpdateWithoutUserDataInput;
  create: DocumentCreateWithoutUserInput;
}

export interface DocumentWhereInput {
  AND?: DocumentWhereInput[] | DocumentWhereInput | null;
  OR?: DocumentWhereInput[] | DocumentWhereInput | null;
  NOT?: DocumentWhereInput[] | DocumentWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  name?: String | null;
  name_not?: String | null;
  name_in?: String[] | String | null;
  name_not_in?: String[] | String | null;
  name_lt?: String | null;
  name_lte?: String | null;
  name_gt?: String | null;
  name_gte?: String | null;
  name_contains?: String | null;
  name_not_contains?: String | null;
  name_starts_with?: String | null;
  name_not_starts_with?: String | null;
  name_ends_with?: String | null;
  name_not_ends_with?: String | null;
  filename?: String | null;
  filename_not?: String | null;
  filename_in?: String[] | String | null;
  filename_not_in?: String[] | String | null;
  filename_lt?: String | null;
  filename_lte?: String | null;
  filename_gt?: String | null;
  filename_gte?: String | null;
  filename_contains?: String | null;
  filename_not_contains?: String | null;
  filename_starts_with?: String | null;
  filename_not_starts_with?: String | null;
  filename_ends_with?: String | null;
  filename_not_ends_with?: String | null;
  type?: DocumentType | null;
  type_not?: DocumentType | null;
  type_in?: DocumentType[] | DocumentType | null;
  type_not_in?: DocumentType[] | DocumentType | null;
  updatedAt?: DateTime | null;
  updatedAt_not?: DateTime | null;
  updatedAt_in?: DateTime[] | DateTime | null;
  updatedAt_not_in?: DateTime[] | DateTime | null;
  updatedAt_lt?: DateTime | null;
  updatedAt_lte?: DateTime | null;
  updatedAt_gt?: DateTime | null;
  updatedAt_gte?: DateTime | null;
  createdAt?: DateTime | null;
  createdAt_not?: DateTime | null;
  createdAt_in?: DateTime[] | DateTime | null;
  createdAt_not_in?: DateTime[] | DateTime | null;
  createdAt_lt?: DateTime | null;
  createdAt_lte?: DateTime | null;
  createdAt_gt?: DateTime | null;
  createdAt_gte?: DateTime | null;
  user?: UserWhereInput | null;
  metrics_every?: MetricWhereInput | null;
  metrics_some?: MetricWhereInput | null;
  metrics_none?: MetricWhereInput | null;
  spendingEntries_every?: SpendingEntryWhereInput | null;
  spendingEntries_some?: SpendingEntryWhereInput | null;
  spendingEntries_none?: SpendingEntryWhereInput | null;
}

export interface DocumentWhereUniqueInput {
  id?: ID_Input | null;
  filename?: String | null;
}

export interface MetricCreateInput {
  id?: ID_Input | null;
  value: Float;
  type: SpendingType;
  document: DocumentCreateOneWithoutMetricsInput;
}

export interface MetricCreateManyWithoutDocumentInput {
  create?:
    | MetricCreateWithoutDocumentInput[]
    | MetricCreateWithoutDocumentInput
    | null;
  connect?: MetricWhereUniqueInput[] | MetricWhereUniqueInput | null;
}

export interface MetricCreateWithoutDocumentInput {
  id?: ID_Input | null;
  value: Float;
  type: SpendingType;
}

export interface MetricScalarWhereInput {
  AND?: MetricScalarWhereInput[] | MetricScalarWhereInput | null;
  OR?: MetricScalarWhereInput[] | MetricScalarWhereInput | null;
  NOT?: MetricScalarWhereInput[] | MetricScalarWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  value?: Float | null;
  value_not?: Float | null;
  value_in?: Float[] | Float | null;
  value_not_in?: Float[] | Float | null;
  value_lt?: Float | null;
  value_lte?: Float | null;
  value_gt?: Float | null;
  value_gte?: Float | null;
  type?: SpendingType | null;
  type_not?: SpendingType | null;
  type_in?: SpendingType[] | SpendingType | null;
  type_not_in?: SpendingType[] | SpendingType | null;
}

export interface MetricSubscriptionWhereInput {
  AND?: MetricSubscriptionWhereInput[] | MetricSubscriptionWhereInput | null;
  OR?: MetricSubscriptionWhereInput[] | MetricSubscriptionWhereInput | null;
  NOT?: MetricSubscriptionWhereInput[] | MetricSubscriptionWhereInput | null;
  mutation_in?: MutationType[] | MutationType | null;
  updatedFields_contains?: String | null;
  updatedFields_contains_every?: String[] | String | null;
  updatedFields_contains_some?: String[] | String | null;
  node?: MetricWhereInput | null;
}

export interface MetricUpdateInput {
  value?: Float | null;
  type?: SpendingType | null;
  document?: DocumentUpdateOneRequiredWithoutMetricsInput | null;
}

export interface MetricUpdateManyDataInput {
  value?: Float | null;
  type?: SpendingType | null;
}

export interface MetricUpdateManyMutationInput {
  value?: Float | null;
  type?: SpendingType | null;
}

export interface MetricUpdateManyWithoutDocumentInput {
  create?:
    | MetricCreateWithoutDocumentInput[]
    | MetricCreateWithoutDocumentInput
    | null;
  connect?: MetricWhereUniqueInput[] | MetricWhereUniqueInput | null;
  set?: MetricWhereUniqueInput[] | MetricWhereUniqueInput | null;
  disconnect?: MetricWhereUniqueInput[] | MetricWhereUniqueInput | null;
  delete?: MetricWhereUniqueInput[] | MetricWhereUniqueInput | null;
  update?:
    | MetricUpdateWithWhereUniqueWithoutDocumentInput[]
    | MetricUpdateWithWhereUniqueWithoutDocumentInput
    | null;
  updateMany?:
    | MetricUpdateManyWithWhereNestedInput[]
    | MetricUpdateManyWithWhereNestedInput
    | null;
  deleteMany?: MetricScalarWhereInput[] | MetricScalarWhereInput | null;
  upsert?:
    | MetricUpsertWithWhereUniqueWithoutDocumentInput[]
    | MetricUpsertWithWhereUniqueWithoutDocumentInput
    | null;
}

export interface MetricUpdateManyWithWhereNestedInput {
  where: MetricScalarWhereInput;
  data: MetricUpdateManyDataInput;
}

export interface MetricUpdateWithoutDocumentDataInput {
  value?: Float | null;
  type?: SpendingType | null;
}

export interface MetricUpdateWithWhereUniqueWithoutDocumentInput {
  where: MetricWhereUniqueInput;
  data: MetricUpdateWithoutDocumentDataInput;
}

export interface MetricUpsertWithWhereUniqueWithoutDocumentInput {
  where: MetricWhereUniqueInput;
  update: MetricUpdateWithoutDocumentDataInput;
  create: MetricCreateWithoutDocumentInput;
}

export interface MetricWhereInput {
  AND?: MetricWhereInput[] | MetricWhereInput | null;
  OR?: MetricWhereInput[] | MetricWhereInput | null;
  NOT?: MetricWhereInput[] | MetricWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  value?: Float | null;
  value_not?: Float | null;
  value_in?: Float[] | Float | null;
  value_not_in?: Float[] | Float | null;
  value_lt?: Float | null;
  value_lte?: Float | null;
  value_gt?: Float | null;
  value_gte?: Float | null;
  type?: SpendingType | null;
  type_not?: SpendingType | null;
  type_in?: SpendingType[] | SpendingType | null;
  type_not_in?: SpendingType[] | SpendingType | null;
  document?: DocumentWhereInput | null;
}

export interface MetricWhereUniqueInput {
  id?: ID_Input | null;
}

export interface SpendingEntryCreateInput {
  id?: ID_Input | null;
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  category?: SpendingType | null;
  document?: DocumentCreateOneWithoutSpendingEntriesInput | null;
}

export interface SpendingEntryCreateManyWithoutDocumentInput {
  create?:
    | SpendingEntryCreateWithoutDocumentInput[]
    | SpendingEntryCreateWithoutDocumentInput
    | null;
  connect?:
    | SpendingEntryWhereUniqueInput[]
    | SpendingEntryWhereUniqueInput
    | null;
}

export interface SpendingEntryCreateWithoutDocumentInput {
  id?: ID_Input | null;
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  category?: SpendingType | null;
}

export interface SpendingEntryScalarWhereInput {
  AND?: SpendingEntryScalarWhereInput[] | SpendingEntryScalarWhereInput | null;
  OR?: SpendingEntryScalarWhereInput[] | SpendingEntryScalarWhereInput | null;
  NOT?: SpendingEntryScalarWhereInput[] | SpendingEntryScalarWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  reference?: String | null;
  reference_not?: String | null;
  reference_in?: String[] | String | null;
  reference_not_in?: String[] | String | null;
  reference_lt?: String | null;
  reference_lte?: String | null;
  reference_gt?: String | null;
  reference_gte?: String | null;
  reference_contains?: String | null;
  reference_not_contains?: String | null;
  reference_starts_with?: String | null;
  reference_not_starts_with?: String | null;
  reference_ends_with?: String | null;
  reference_not_ends_with?: String | null;
  designation?: String | null;
  designation_not?: String | null;
  designation_in?: String[] | String | null;
  designation_not_in?: String[] | String | null;
  designation_lt?: String | null;
  designation_lte?: String | null;
  designation_gt?: String | null;
  designation_gte?: String | null;
  designation_contains?: String | null;
  designation_not_contains?: String | null;
  designation_starts_with?: String | null;
  designation_not_starts_with?: String | null;
  designation_ends_with?: String | null;
  designation_not_ends_with?: String | null;
  date?: DateTime | null;
  date_not?: DateTime | null;
  date_in?: DateTime[] | DateTime | null;
  date_not_in?: DateTime[] | DateTime | null;
  date_lt?: DateTime | null;
  date_lte?: DateTime | null;
  date_gt?: DateTime | null;
  date_gte?: DateTime | null;
  paymentMode?: String | null;
  paymentMode_not?: String | null;
  paymentMode_in?: String[] | String | null;
  paymentMode_not_in?: String[] | String | null;
  paymentMode_lt?: String | null;
  paymentMode_lte?: String | null;
  paymentMode_gt?: String | null;
  paymentMode_gte?: String | null;
  paymentMode_contains?: String | null;
  paymentMode_not_contains?: String | null;
  paymentMode_starts_with?: String | null;
  paymentMode_not_starts_with?: String | null;
  paymentMode_ends_with?: String | null;
  paymentMode_not_ends_with?: String | null;
  supplier?: String | null;
  supplier_not?: String | null;
  supplier_in?: String[] | String | null;
  supplier_not_in?: String[] | String | null;
  supplier_lt?: String | null;
  supplier_lte?: String | null;
  supplier_gt?: String | null;
  supplier_gte?: String | null;
  supplier_contains?: String | null;
  supplier_not_contains?: String | null;
  supplier_starts_with?: String | null;
  supplier_not_starts_with?: String | null;
  supplier_ends_with?: String | null;
  supplier_not_ends_with?: String | null;
  amount?: Float | null;
  amount_not?: Float | null;
  amount_in?: Float[] | Float | null;
  amount_not_in?: Float[] | Float | null;
  amount_lt?: Float | null;
  amount_lte?: Float | null;
  amount_gt?: Float | null;
  amount_gte?: Float | null;
  description?: String | null;
  description_not?: String | null;
  description_in?: String[] | String | null;
  description_not_in?: String[] | String | null;
  description_lt?: String | null;
  description_lte?: String | null;
  description_gt?: String | null;
  description_gte?: String | null;
  description_contains?: String | null;
  description_not_contains?: String | null;
  description_starts_with?: String | null;
  description_not_starts_with?: String | null;
  description_ends_with?: String | null;
  description_not_ends_with?: String | null;
  observation?: String | null;
  observation_not?: String | null;
  observation_in?: String[] | String | null;
  observation_not_in?: String[] | String | null;
  observation_lt?: String | null;
  observation_lte?: String | null;
  observation_gt?: String | null;
  observation_gte?: String | null;
  observation_contains?: String | null;
  observation_not_contains?: String | null;
  observation_starts_with?: String | null;
  observation_not_starts_with?: String | null;
  observation_ends_with?: String | null;
  observation_not_ends_with?: String | null;
  category?: SpendingType | null;
  category_not?: SpendingType | null;
  category_in?: SpendingType[] | SpendingType | null;
  category_not_in?: SpendingType[] | SpendingType | null;
}

export interface SpendingEntrySubscriptionWhereInput {
  AND?:
    | SpendingEntrySubscriptionWhereInput[]
    | SpendingEntrySubscriptionWhereInput
    | null;
  OR?:
    | SpendingEntrySubscriptionWhereInput[]
    | SpendingEntrySubscriptionWhereInput
    | null;
  NOT?:
    | SpendingEntrySubscriptionWhereInput[]
    | SpendingEntrySubscriptionWhereInput
    | null;
  mutation_in?: MutationType[] | MutationType | null;
  updatedFields_contains?: String | null;
  updatedFields_contains_every?: String[] | String | null;
  updatedFields_contains_some?: String[] | String | null;
  node?: SpendingEntryWhereInput | null;
}

export interface SpendingEntryUpdateInput {
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  category?: SpendingType | null;
  document?: DocumentUpdateOneWithoutSpendingEntriesInput | null;
}

export interface SpendingEntryUpdateManyDataInput {
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  category?: SpendingType | null;
}

export interface SpendingEntryUpdateManyMutationInput {
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  category?: SpendingType | null;
}

export interface SpendingEntryUpdateManyWithoutDocumentInput {
  create?:
    | SpendingEntryCreateWithoutDocumentInput[]
    | SpendingEntryCreateWithoutDocumentInput
    | null;
  connect?:
    | SpendingEntryWhereUniqueInput[]
    | SpendingEntryWhereUniqueInput
    | null;
  set?: SpendingEntryWhereUniqueInput[] | SpendingEntryWhereUniqueInput | null;
  disconnect?:
    | SpendingEntryWhereUniqueInput[]
    | SpendingEntryWhereUniqueInput
    | null;
  delete?:
    | SpendingEntryWhereUniqueInput[]
    | SpendingEntryWhereUniqueInput
    | null;
  update?:
    | SpendingEntryUpdateWithWhereUniqueWithoutDocumentInput[]
    | SpendingEntryUpdateWithWhereUniqueWithoutDocumentInput
    | null;
  updateMany?:
    | SpendingEntryUpdateManyWithWhereNestedInput[]
    | SpendingEntryUpdateManyWithWhereNestedInput
    | null;
  deleteMany?:
    | SpendingEntryScalarWhereInput[]
    | SpendingEntryScalarWhereInput
    | null;
  upsert?:
    | SpendingEntryUpsertWithWhereUniqueWithoutDocumentInput[]
    | SpendingEntryUpsertWithWhereUniqueWithoutDocumentInput
    | null;
}

export interface SpendingEntryUpdateManyWithWhereNestedInput {
  where: SpendingEntryScalarWhereInput;
  data: SpendingEntryUpdateManyDataInput;
}

export interface SpendingEntryUpdateWithoutDocumentDataInput {
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  category?: SpendingType | null;
}

export interface SpendingEntryUpdateWithWhereUniqueWithoutDocumentInput {
  where: SpendingEntryWhereUniqueInput;
  data: SpendingEntryUpdateWithoutDocumentDataInput;
}

export interface SpendingEntryUpsertWithWhereUniqueWithoutDocumentInput {
  where: SpendingEntryWhereUniqueInput;
  update: SpendingEntryUpdateWithoutDocumentDataInput;
  create: SpendingEntryCreateWithoutDocumentInput;
}

export interface SpendingEntryWhereInput {
  AND?: SpendingEntryWhereInput[] | SpendingEntryWhereInput | null;
  OR?: SpendingEntryWhereInput[] | SpendingEntryWhereInput | null;
  NOT?: SpendingEntryWhereInput[] | SpendingEntryWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  reference?: String | null;
  reference_not?: String | null;
  reference_in?: String[] | String | null;
  reference_not_in?: String[] | String | null;
  reference_lt?: String | null;
  reference_lte?: String | null;
  reference_gt?: String | null;
  reference_gte?: String | null;
  reference_contains?: String | null;
  reference_not_contains?: String | null;
  reference_starts_with?: String | null;
  reference_not_starts_with?: String | null;
  reference_ends_with?: String | null;
  reference_not_ends_with?: String | null;
  designation?: String | null;
  designation_not?: String | null;
  designation_in?: String[] | String | null;
  designation_not_in?: String[] | String | null;
  designation_lt?: String | null;
  designation_lte?: String | null;
  designation_gt?: String | null;
  designation_gte?: String | null;
  designation_contains?: String | null;
  designation_not_contains?: String | null;
  designation_starts_with?: String | null;
  designation_not_starts_with?: String | null;
  designation_ends_with?: String | null;
  designation_not_ends_with?: String | null;
  date?: DateTime | null;
  date_not?: DateTime | null;
  date_in?: DateTime[] | DateTime | null;
  date_not_in?: DateTime[] | DateTime | null;
  date_lt?: DateTime | null;
  date_lte?: DateTime | null;
  date_gt?: DateTime | null;
  date_gte?: DateTime | null;
  paymentMode?: String | null;
  paymentMode_not?: String | null;
  paymentMode_in?: String[] | String | null;
  paymentMode_not_in?: String[] | String | null;
  paymentMode_lt?: String | null;
  paymentMode_lte?: String | null;
  paymentMode_gt?: String | null;
  paymentMode_gte?: String | null;
  paymentMode_contains?: String | null;
  paymentMode_not_contains?: String | null;
  paymentMode_starts_with?: String | null;
  paymentMode_not_starts_with?: String | null;
  paymentMode_ends_with?: String | null;
  paymentMode_not_ends_with?: String | null;
  supplier?: String | null;
  supplier_not?: String | null;
  supplier_in?: String[] | String | null;
  supplier_not_in?: String[] | String | null;
  supplier_lt?: String | null;
  supplier_lte?: String | null;
  supplier_gt?: String | null;
  supplier_gte?: String | null;
  supplier_contains?: String | null;
  supplier_not_contains?: String | null;
  supplier_starts_with?: String | null;
  supplier_not_starts_with?: String | null;
  supplier_ends_with?: String | null;
  supplier_not_ends_with?: String | null;
  amount?: Float | null;
  amount_not?: Float | null;
  amount_in?: Float[] | Float | null;
  amount_not_in?: Float[] | Float | null;
  amount_lt?: Float | null;
  amount_lte?: Float | null;
  amount_gt?: Float | null;
  amount_gte?: Float | null;
  description?: String | null;
  description_not?: String | null;
  description_in?: String[] | String | null;
  description_not_in?: String[] | String | null;
  description_lt?: String | null;
  description_lte?: String | null;
  description_gt?: String | null;
  description_gte?: String | null;
  description_contains?: String | null;
  description_not_contains?: String | null;
  description_starts_with?: String | null;
  description_not_starts_with?: String | null;
  description_ends_with?: String | null;
  description_not_ends_with?: String | null;
  observation?: String | null;
  observation_not?: String | null;
  observation_in?: String[] | String | null;
  observation_not_in?: String[] | String | null;
  observation_lt?: String | null;
  observation_lte?: String | null;
  observation_gt?: String | null;
  observation_gte?: String | null;
  observation_contains?: String | null;
  observation_not_contains?: String | null;
  observation_starts_with?: String | null;
  observation_not_starts_with?: String | null;
  observation_ends_with?: String | null;
  observation_not_ends_with?: String | null;
  category?: SpendingType | null;
  category_not?: SpendingType | null;
  category_in?: SpendingType[] | SpendingType | null;
  category_not_in?: SpendingType[] | SpendingType | null;
  document?: DocumentWhereInput | null;
}

export interface SpendingEntryWhereUniqueInput {
  id?: ID_Input | null;
}

export interface UserCreateInput {
  id?: ID_Input | null;
  mail: String;
  name: String;
  password: String;
  agendas?: AgendaCreateManyWithoutUserInput | null;
  documents?: DocumentCreateManyWithoutUserInput | null;
}

export interface UserCreateOneWithoutAgendasInput {
  create?: UserCreateWithoutAgendasInput | null;
  connect?: UserWhereUniqueInput | null;
}

export interface UserCreateOneWithoutDocumentsInput {
  create?: UserCreateWithoutDocumentsInput | null;
  connect?: UserWhereUniqueInput | null;
}

export interface UserCreateWithoutAgendasInput {
  id?: ID_Input | null;
  mail: String;
  name: String;
  password: String;
  documents?: DocumentCreateManyWithoutUserInput | null;
}

export interface UserCreateWithoutDocumentsInput {
  id?: ID_Input | null;
  mail: String;
  name: String;
  password: String;
  agendas?: AgendaCreateManyWithoutUserInput | null;
}

export interface UserSubscriptionWhereInput {
  AND?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null;
  OR?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null;
  NOT?: UserSubscriptionWhereInput[] | UserSubscriptionWhereInput | null;
  mutation_in?: MutationType[] | MutationType | null;
  updatedFields_contains?: String | null;
  updatedFields_contains_every?: String[] | String | null;
  updatedFields_contains_some?: String[] | String | null;
  node?: UserWhereInput | null;
}

export interface UserUpdateInput {
  mail?: String | null;
  name?: String | null;
  password?: String | null;
  agendas?: AgendaUpdateManyWithoutUserInput | null;
  documents?: DocumentUpdateManyWithoutUserInput | null;
}

export interface UserUpdateManyMutationInput {
  mail?: String | null;
  name?: String | null;
  password?: String | null;
}

export interface UserUpdateOneRequiredWithoutAgendasInput {
  create?: UserCreateWithoutAgendasInput | null;
  connect?: UserWhereUniqueInput | null;
  update?: UserUpdateWithoutAgendasDataInput | null;
  upsert?: UserUpsertWithoutAgendasInput | null;
}

export interface UserUpdateOneRequiredWithoutDocumentsInput {
  create?: UserCreateWithoutDocumentsInput | null;
  connect?: UserWhereUniqueInput | null;
  update?: UserUpdateWithoutDocumentsDataInput | null;
  upsert?: UserUpsertWithoutDocumentsInput | null;
}

export interface UserUpdateWithoutAgendasDataInput {
  mail?: String | null;
  name?: String | null;
  password?: String | null;
  documents?: DocumentUpdateManyWithoutUserInput | null;
}

export interface UserUpdateWithoutDocumentsDataInput {
  mail?: String | null;
  name?: String | null;
  password?: String | null;
  agendas?: AgendaUpdateManyWithoutUserInput | null;
}

export interface UserUpsertWithoutAgendasInput {
  update: UserUpdateWithoutAgendasDataInput;
  create: UserCreateWithoutAgendasInput;
}

export interface UserUpsertWithoutDocumentsInput {
  update: UserUpdateWithoutDocumentsDataInput;
  create: UserCreateWithoutDocumentsInput;
}

export interface UserWhereInput {
  AND?: UserWhereInput[] | UserWhereInput | null;
  OR?: UserWhereInput[] | UserWhereInput | null;
  NOT?: UserWhereInput[] | UserWhereInput | null;
  id?: ID_Input | null;
  id_not?: ID_Input | null;
  id_in?: ID_Output[] | ID_Output | null;
  id_not_in?: ID_Output[] | ID_Output | null;
  id_lt?: ID_Input | null;
  id_lte?: ID_Input | null;
  id_gt?: ID_Input | null;
  id_gte?: ID_Input | null;
  id_contains?: ID_Input | null;
  id_not_contains?: ID_Input | null;
  id_starts_with?: ID_Input | null;
  id_not_starts_with?: ID_Input | null;
  id_ends_with?: ID_Input | null;
  id_not_ends_with?: ID_Input | null;
  mail?: String | null;
  mail_not?: String | null;
  mail_in?: String[] | String | null;
  mail_not_in?: String[] | String | null;
  mail_lt?: String | null;
  mail_lte?: String | null;
  mail_gt?: String | null;
  mail_gte?: String | null;
  mail_contains?: String | null;
  mail_not_contains?: String | null;
  mail_starts_with?: String | null;
  mail_not_starts_with?: String | null;
  mail_ends_with?: String | null;
  mail_not_ends_with?: String | null;
  name?: String | null;
  name_not?: String | null;
  name_in?: String[] | String | null;
  name_not_in?: String[] | String | null;
  name_lt?: String | null;
  name_lte?: String | null;
  name_gt?: String | null;
  name_gte?: String | null;
  name_contains?: String | null;
  name_not_contains?: String | null;
  name_starts_with?: String | null;
  name_not_starts_with?: String | null;
  name_ends_with?: String | null;
  name_not_ends_with?: String | null;
  password?: String | null;
  password_not?: String | null;
  password_in?: String[] | String | null;
  password_not_in?: String[] | String | null;
  password_lt?: String | null;
  password_lte?: String | null;
  password_gt?: String | null;
  password_gte?: String | null;
  password_contains?: String | null;
  password_not_contains?: String | null;
  password_starts_with?: String | null;
  password_not_starts_with?: String | null;
  password_ends_with?: String | null;
  password_not_ends_with?: String | null;
  updatedAt?: DateTime | null;
  updatedAt_not?: DateTime | null;
  updatedAt_in?: DateTime[] | DateTime | null;
  updatedAt_not_in?: DateTime[] | DateTime | null;
  updatedAt_lt?: DateTime | null;
  updatedAt_lte?: DateTime | null;
  updatedAt_gt?: DateTime | null;
  updatedAt_gte?: DateTime | null;
  createdAt?: DateTime | null;
  createdAt_not?: DateTime | null;
  createdAt_in?: DateTime[] | DateTime | null;
  createdAt_not_in?: DateTime[] | DateTime | null;
  createdAt_lt?: DateTime | null;
  createdAt_lte?: DateTime | null;
  createdAt_gt?: DateTime | null;
  createdAt_gte?: DateTime | null;
  agendas_every?: AgendaWhereInput | null;
  agendas_some?: AgendaWhereInput | null;
  agendas_none?: AgendaWhereInput | null;
  documents_every?: DocumentWhereInput | null;
  documents_some?: DocumentWhereInput | null;
  documents_none?: DocumentWhereInput | null;
}

export interface UserWhereUniqueInput {
  id?: ID_Input | null;
  mail?: String | null;
  name?: String | null;
}

/*
 * An object with an ID

 */
export interface Node {
  id: ID_Output;
}

export interface Agenda extends Node {
  id: ID_Output;
  name?: String | null;
  url?: String | null;
  user: User;
  updatedAt: DateTime;
  createdAt: DateTime;
}

/*
 * A connection to a list of items.

 */
export interface AgendaConnection {
  pageInfo: PageInfo;
  edges: Array<AgendaEdge | null>;
  aggregate: AggregateAgenda;
}

/*
 * An edge in a connection.

 */
export interface AgendaEdge {
  node: Agenda;
  cursor: String;
}

export interface AgendaPreviousValues {
  id: ID_Output;
  name?: String | null;
  url?: String | null;
  updatedAt: DateTime;
  createdAt: DateTime;
}

export interface AgendaSubscriptionPayload {
  mutation: MutationType;
  node?: Agenda | null;
  updatedFields?: Array<String> | null;
  previousValues?: AgendaPreviousValues | null;
}

export interface AggregateAgenda {
  count: Int;
}

export interface AggregateDocument {
  count: Int;
}

export interface AggregateMetric {
  count: Int;
}

export interface AggregateSpendingEntry {
  count: Int;
}

export interface AggregateUser {
  count: Int;
}

export interface BatchPayload {
  count: Long;
}

export interface Document extends Node {
  id: ID_Output;
  name?: String | null;
  filename: String;
  user: User;
  type?: DocumentType | null;
  metrics?: Array<Metric> | null;
  spendingEntries?: Array<SpendingEntry> | null;
  updatedAt: DateTime;
  createdAt: DateTime;
}

/*
 * A connection to a list of items.

 */
export interface DocumentConnection {
  pageInfo: PageInfo;
  edges: Array<DocumentEdge | null>;
  aggregate: AggregateDocument;
}

/*
 * An edge in a connection.

 */
export interface DocumentEdge {
  node: Document;
  cursor: String;
}

export interface DocumentPreviousValues {
  id: ID_Output;
  name?: String | null;
  filename: String;
  type?: DocumentType | null;
  updatedAt: DateTime;
  createdAt: DateTime;
}

export interface DocumentSubscriptionPayload {
  mutation: MutationType;
  node?: Document | null;
  updatedFields?: Array<String> | null;
  previousValues?: DocumentPreviousValues | null;
}

export interface Metric extends Node {
  id: ID_Output;
  document: Document;
  value: Float;
  type: SpendingType;
}

/*
 * A connection to a list of items.

 */
export interface MetricConnection {
  pageInfo: PageInfo;
  edges: Array<MetricEdge | null>;
  aggregate: AggregateMetric;
}

/*
 * An edge in a connection.

 */
export interface MetricEdge {
  node: Metric;
  cursor: String;
}

export interface MetricPreviousValues {
  id: ID_Output;
  value: Float;
  type: SpendingType;
}

export interface MetricSubscriptionPayload {
  mutation: MutationType;
  node?: Metric | null;
  updatedFields?: Array<String> | null;
  previousValues?: MetricPreviousValues | null;
}

/*
 * Information about pagination in a connection.

 */
export interface PageInfo {
  hasNextPage: Boolean;
  hasPreviousPage: Boolean;
  startCursor?: String | null;
  endCursor?: String | null;
}

export interface SpendingEntry extends Node {
  id: ID_Output;
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  document?: Document | null;
  category?: SpendingType | null;
}

/*
 * A connection to a list of items.

 */
export interface SpendingEntryConnection {
  pageInfo: PageInfo;
  edges: Array<SpendingEntryEdge | null>;
  aggregate: AggregateSpendingEntry;
}

/*
 * An edge in a connection.

 */
export interface SpendingEntryEdge {
  node: SpendingEntry;
  cursor: String;
}

export interface SpendingEntryPreviousValues {
  id: ID_Output;
  reference?: String | null;
  designation?: String | null;
  date?: DateTime | null;
  paymentMode?: String | null;
  supplier?: String | null;
  amount?: Float | null;
  description?: String | null;
  observation?: String | null;
  category?: SpendingType | null;
}

export interface SpendingEntrySubscriptionPayload {
  mutation: MutationType;
  node?: SpendingEntry | null;
  updatedFields?: Array<String> | null;
  previousValues?: SpendingEntryPreviousValues | null;
}

export interface User extends Node {
  id: ID_Output;
  mail: String;
  name: String;
  password: String;
  agendas?: Array<Agenda> | null;
  documents?: Array<Document> | null;
  updatedAt: DateTime;
  createdAt: DateTime;
}

/*
 * A connection to a list of items.

 */
export interface UserConnection {
  pageInfo: PageInfo;
  edges: Array<UserEdge | null>;
  aggregate: AggregateUser;
}

/*
 * An edge in a connection.

 */
export interface UserEdge {
  node: User;
  cursor: String;
}

export interface UserPreviousValues {
  id: ID_Output;
  mail: String;
  name: String;
  password: String;
  updatedAt: DateTime;
  createdAt: DateTime;
}

export interface UserSubscriptionPayload {
  mutation: MutationType;
  node?: User | null;
  updatedFields?: Array<String> | null;
  previousValues?: UserPreviousValues | null;
}

/*
The `Boolean` scalar type represents `true` or `false`.
*/
export type Boolean = boolean;

export type DateTime = Date | string;

/*
The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](http://en.wikipedia.org/wiki/IEEE_floating_point). 
*/
export type Float = number;

/*
The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.
*/
export type ID_Input = string | number;
export type ID_Output = string;

/*
The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1. 
*/
export type Int = number;

/*
The `Long` scalar type represents non-fractional signed whole numeric values.
Long can represent values between -(2^63) and 2^63 - 1.
*/
export type Long = string;

/*
The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.
*/
export type String = string;
