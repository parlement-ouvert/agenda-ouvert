import { GraphQLResolveInfo } from "graphql";
import * as jwt from "jsonwebtoken";

import { Prisma as PrismaClient } from "./generated/prisma-client";
import { Prisma as PrismaBinding } from "./generated/prisma-binding";

export interface Context {
  binding: PrismaBinding;
  client: PrismaClient;
  request?: any;
  response?: any;
  connection?: any;
}

export const getUserId = (context: Context, authorization: String = null) => {
  // Token appears in different places depending
  // on whether the request is HTTP or WS
  const Authorization = authorization
    ? authorization
    : context.request
    ? context.request.get("Authorization")
    : context.connection.context.Authorization || null;

  if (Authorization) {
    const token = Authorization.replace("Bearer ", "");
    const { userId } = jwt.verify(token, process.env.APP_SECRET) as {
      userId: string;
    };
    return userId;
  }

  throw new AuthError();
};

export class AuthError extends Error {
  constructor() {
    super("Not authenticated");
  }
}

export class CustomError extends Error {
  constructor(message: string) {
    super(message);
  }
}
