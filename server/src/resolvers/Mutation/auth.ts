import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";
import * as nodemailer from "nodemailer";
import { Context, CustomError } from "../../utils";

let rw = require("random-word-fr");

export const authMutation = {
  async signup(parent, { mail }, context: Context) {
    if (!mail.endsWith("assemblee-nationale.fr")) {
      throw new CustomError("Adresse email non valable");
    }

    // Generate password and create user in database
    const clearPassword = [rw(), rw(), rw()].join("-");
    const password = await bcrypt.hash(clearPassword, 10);
    const user = await context.client.createUser({
      mail,
      name: mail.split("@")[0],
      password
    });

    // Send password to email address
    const transporter = nodemailer.createTransport({
      host: process.env.SMTP_SERVER,
      port: 587,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD
      }
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
      from: '"Fred Foo 👻" <foo@example.com>',
      to: mail,
      subject: "Inscription à Agenda Ouvert - Votre mot de passe",
      text: password,
      html: password
    });

    console.log("Message sent: %s", info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

    return {
      token: jwt.sign({ userId: user.id }, process.env.APP_SECRET),
      user
    };
  },

  async login(parent, { mail, password }, context: Context) {
    const user = await context.client.user({ mail });
    if (!user) {
      throw new Error(
        `Aucun compte n'a encore été crée pour l'adresse ${mail}`
      );
    }

    const valid = await bcrypt.compare(password, user.password);
    if (!valid) {
      throw new Error("Mot de passe incorrect.");
    }

    return {
      token: jwt.sign({ userId: user.id }, process.env.APP_SECRET),
      user
    };
  }
};
