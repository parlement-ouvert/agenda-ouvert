import { forwardTo } from "prisma-binding";

import { Context, CustomError, getUserId } from "../../utils";

export const mutations = {
  addAgenda(parent, {}, context: Context) {
    const id = getUserId(context);
    return context.client.createAgenda({ user: { connect: { id } } });
  },
  updateAgenda(parent, { id, data }, context: Context) {
    getUserId(context);
    return context.client.updateAgenda({
      data,
      where: {
        id
      }
    });
  },
  async deleteAgenda(parent, { id }, context: Context) {
    const userId = getUserId(context);
    // Check that user owns agenda
    const user = await context.client.agenda({ id }).user();
    if (userId !== user.id) {
      throw new CustomError(
        "Vous essayez de supprimer un agenda qui ne vous appartient pas."
      );
    }

    return context.client.deleteAgenda({ id });
  },
  async deleteDocument(parent, { id }, context: Context) {
    const userId = getUserId(context);

    const user = await context.client.document({ id }).user();
    if (userId !== user.id) {
      throw new CustomError(
        "Vous essayez de supprimer un document qui ne vous appartient pas."
      );
    }

    return context.client.deleteDocument({ id });
  }
};
