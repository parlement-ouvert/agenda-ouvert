import * as jwt from "jsonwebtoken";
import { forwardTo } from "prisma-binding";

import { Context, getUserId } from "../../utils";

export const queries = {
  // Binded end-points
  users: forwardTo("binding"),
  metrics: forwardTo("binding"),
  // Client end-points
  agendas(parent, {}, context: Context) {
    const id = getUserId(context);
    return context.client.user({ id }).agendas({ orderBy: "createdAt_DESC" });
  },
  documents(parent, {}, context: Context) {
    const id = getUserId(context);
    return context.client.user({ id }).documents({ orderBy: "createdAt_DESC" });
  },
  async deputyDocuments(parent, { id }, context: Context) {
    const query = `
    query($id: ID!) {
      documents(where: {user: {id: $id}}) {
        id
        name
        type
        createdAt
        metrics {
          id
          value
          type
        }
        spendingEntries {
          id
          date
          amount
          description
          supplier
          category
        }
      }
    }
    `;

    const documents = await context.client.$graphql(query, { id });
    return documents.documents;
  },
  async deputyAgendas(parent, { id }, context: Context) {
    const query = `
    query($id: ID!) {
      agendas(where: {user: {id: $id}}) {
        id
        name
        url
      }
    }
    `;

    const agendas = await context.client.$graphql(query, { id });
    return agendas.agendas;
  },
  async availableAgendas(parent, {}, context: Context) {
    // Fetch users
    const users = await context.client.users();

    // Iterate through users to fetch agendas
    const agendasList = await Promise.all(
      users.map(
        async (user: any) =>
          await context.client.user({ id: user.id }).agendas()
      )
    );

    // Flatten list of lists of agendas
    let agendasFlat = [];
    for (const agendas of agendasList) {
      agendasFlat = [...agendasFlat, ...agendas];
    }

    return agendasFlat;
  },
  isAuthenticated(parent, args, context: Context) {
    const Authorization = context.request
      ? context.request.get("Authorization")
      : context.connection.context.Authorization || null;

    if (Authorization) {
      const token = Authorization.replace("Bearer ", "");

      try {
        const { userId } = jwt.verify(token, process.env.APP_SECRET) as {
          userId: string;
        };
      } catch (e) {
        return false;
      }

      return true;
    }

    return false;
  },
  user(parent, args, context: Context) {
    const id = getUserId(context);

    return context.client.user({ id });
  }
};
