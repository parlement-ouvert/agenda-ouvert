import { authMutation } from "./Mutation/auth";
import { mutations } from "./Mutation/main";
import { queries } from "./Query/main";

export default {
  Query: {
    ...queries
  },
  Mutation: {
    ...authMutation,
    ...mutations
  }
};
