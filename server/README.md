# Agenda-ouvert `Server`

## Installation

```
yarn install
env $(cat .env.staging) docker-compose up
yarn run prisma deploy -e .env.staging
yarn run prisma seed -e .env.staging
```

```
yarn start
```
