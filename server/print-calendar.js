// call it with: `node print-calendar https://url-of-ics-file.ics
"use strict";

const ical = require("ical");
const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];

ical.fromURL(process.argv[2], {}, function(err, data) {
  for (let k in data) {
    if (data.hasOwnProperty(k)) {
      var ev = data[k];
      if (data[k].type == "VEVENT") {
        console.log(
          `${ev.summary} is in ${ev.location} on the ${ev.start.getDate()} of ${
            months[ev.start.getMonth()]
          } at ${ev.start.toLocaleTimeString("en-GB")}`
        );
      }
    }
  }
});
