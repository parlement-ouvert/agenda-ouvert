const { prisma } = require("../src/generated/prisma-client/");

async function main() {
  await prisma.createUser({
    mail: "simone.veil@assemblee-nationale.fr",
    password: "$2a$10$.nNnVCp80u9HaQ0ZMqXkt.pr1pxplyis0cp6/zY2cpQ4GWE8bHoSS", // hashed password: "secret"
    name: "Simone Veil",
    agendas: {
      create: [
        {
          name: "Google Calendar Test",
          url:
            "https://calendar.google.com/calendar/ical/alexishault%40gmail.com/public/basic.ics"
        }
      ]
    }
  });

  await prisma.createUser({
    mail: "victor.hugo@assemblee-nationale.fr",
    password: "$2a$10$.nNnVCp80u9HaQ0ZMqXkt.pr1pxplyis0cp6/zY2cpQ4GWE8bHoSS", // hashed password: "secret"
    name: "Victor Hugo",
    agendas: {
      create: [
        {
          name: "Google Calendar Test",
          url:
            "https://calendar.google.com/calendar/ical/alexishault%40gmail.com/public/basic.ics"
        }
      ]
    }
  });
}

main().catch(e => console.error(e));
